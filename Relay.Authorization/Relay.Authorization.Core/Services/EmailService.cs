﻿using System.Net.Mail;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Authorization.Auth.Services
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            var from = "noreply@ihandover.co";
            var pass = "Ball00n$%";

            SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(@from, pass),
                EnableSsl = true
            };

            var mail = new MailMessage(from, message.Destination)
            {
                Subject = message.Subject,
                Body = message.Body,
                IsBodyHtml = true
            };
  
            return client.SendMailAsync(mail);
        }
    }
}
