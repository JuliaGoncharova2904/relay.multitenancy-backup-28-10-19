﻿using System.Web.Configuration;

namespace MomentumPlus.Relay.Authorization.Constants
{
    public static class StripeConstants
    {
        //public static string StripeApiKey = "sk_test_LFwzGhG9T8dMikhMMjaQ1urD";

        public static string StripeApiKey = WebConfigurationManager.AppSettings["stripeKey"];
    }
}
