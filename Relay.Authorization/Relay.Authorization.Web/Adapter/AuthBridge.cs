﻿using System;
using Owin;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Authorization.Adapter;
using MomentumPlus.Relay.Authorization.Auth;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MomentumPlus.Relay.Helpers;
using MomentumPlus.Relay.Authorization.Domain;
using MomentumPlus.Relay.Authorization.Domain.Entities;

namespace MomentumPlus.Relay.Auth
{
    public class AuthBridge : IAuthBridge
    {
        private IAuthService _authService;
        private IRestrictionProvider _restrictionProvider;

        public IAuthService AuthService => _authService ?? (_authService = new AuthService());

        public IRestrictionProvider RestrictionProvider => _restrictionProvider ?? (_restrictionProvider = new RestrictionProvider(AuthService));

        private MultiTenancyContext DbContext => HttpContext.Current.GetOwinContext().Get<MultiTenancyContext>();

        public string GetActiveRelayDbConnectionString()
        {
            try
            {
                Guid? sessionCurrentSubscriptionId = AppSession.Current.CurrentSubscriptionId;

                if (sessionCurrentSubscriptionId.HasValue)
                {
                    Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == sessionCurrentSubscriptionId);

                    var connection = ConfigurationManager.ConnectionStrings["RelayConnection"].ConnectionString;

                    return string.Format(connection, subscription?.RelayDbConnection);
                }
                else
                {
                    var currentUserId = Guid.Parse(HttpContext.Current.User.Identity.GetUserId());

                    RelayUser user = DbContext.Users.FirstOrDefault(s => s.Id == currentUserId);

                    var connection = ConfigurationManager.ConnectionStrings["RelayConnection"].ConnectionString;

                    if (user != null)
                    {
                        AppSession.Current.CurrentSubscriptionId = user.Subscription.Id;
                    }
                    return string.Format(connection, user?.Subscription?.RelayDbConnection);
                }
            }
            catch (NullReferenceException ex)
            {
                var error = ex.ToString();
                return ConfigurationManager.ConnectionStrings["MultiTenancyConnection"].ConnectionString;

            }

            catch (Exception ex)
            {
                var error = ex.ToString();
                return ConfigurationManager.ConnectionStrings["MultiTenancyConnection"].ConnectionString;
            }
        }

        public IEnumerable<string> GetAllRelayDbConnectionStrings()
        {
            var jobContext = new MultiTenancyContext();

            var subscriptionsConnections = jobContext.Subscriptions.Select(subscription => subscription.RelayDbConnection).ToList();

            var connectionStrings = new List<string>();

            if (subscriptionsConnections.Any())
            {
                foreach (var subscriptionConnection in subscriptionsConnections)
                {
                    var connection = ConfigurationManager.ConnectionStrings["RelayConnection"].ConnectionString;

                    connectionStrings.Add(string.Format(connection, subscriptionConnection));
                }
            }

            return connectionStrings;
        }

        public void RegisterAuthModule(IAppBuilder app)
        {
            AuthModuleConfiguration.Apply(app);
        }
    }
}