﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Roles;
using System;
using System.Linq;
using System.Web;
using MomentumPlus.Relay.Authorization.Auth.Managers;
using MomentumPlus.Relay.Authorization.Domain;
using MomentumPlus.Relay.Authorization.Domain.Entities;
using System.Data.Entity.SqlServer;
using MomentumPlus.Relay.Models;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Authorization.Adapter
{ 
    public class RestrictionProvider : IRestrictionProvider
    {
        private readonly SubscriptionRestrictions restrictions;
        private readonly IAuthService _authService;
        private MultiTenancyContext DbContext => HttpContext.Current.GetOwinContext().Get<MultiTenancyContext>();
        private RelayUserManager UserManager => HttpContext.Current.GetOwinContext().Get<RelayUserManager>();
        

        public RestrictionProvider(IAuthService _authService)
        {
            RelayUser user = this.UserManager.FindById(Guid.Parse(HttpContext.Current.User.Identity.GetUserId()));
            restrictions = user.Subscription?.Restrictions;
            this._authService = _authService;
        }

        public bool CarryForwardsLimitExceeded(int carryForwardsNumber)
        {
            if (this.restrictions != null)
            {
                var unlMode = this.restrictions.MaxCarryForwards == null;

                return !unlMode && this.restrictions.MaxCarryForwards <= carryForwardsNumber;
            }

            return true;
        }

        public bool HandoverItemsLimitExceeded(int handoverItemsNumber)
        {
            if (this.restrictions != null)
            {
              //  var unlMode = this.restrictions.MaxHandoverItems == null;

                return !(this.restrictions.MaxHandoverItems == null) && this.restrictions.MaxHandoverItems <= handoverItemsNumber;
            }
            else
            {
                var ownerId = _authService.GetSubscriptionOwnerId();

                if (ownerId.HasValue)
                {
                    var restrictions = this.UserManager.FindById(ownerId.Value).Subscription?.Restrictions;

                    if (restrictions != null)
                    {
                        /*Guid subscriptionIdNew = this.UserManager.FindById(ownerId.Value).SubscriptionId.Value;

                        int usersCountNew = this.DbContext.RelayUserRoles.Where(r => r.Role.Name != iHandoverRoles.Relay.iHandoverAdmin)
                                                                                                            .Where(u => !u.User.LockoutEndDateUtc.HasValue).Select(r => r.User)
                                                                                                             .Count(u => u.SubscriptionId == subscriptionIdNew);

                        return restrictions.MaxUsers <= usersCountNew;*/
                        return !(restrictions.MaxHandoverItems == null) && restrictions.MaxHandoverItems <= handoverItemsNumber;
                    }
                }
            }

            return true;
        }

        public bool HandoverTasksLimitExceeded(int handoverTasksNumber)
        {
            if (this.restrictions != null)
            {
                var unlMode = this.restrictions.MaxHandoverTasks == null;

                return !unlMode && this.restrictions.MaxHandoverTasks <= handoverTasksNumber;
            }

            return true;
        }

        public bool IsMonthArchivesLimited()
        {
            return this.restrictions?.MaxMonthArchives != null;
        }

        public bool ManagersLimitExceeded(Guid userId)
        {
            if (this.restrictions == null) return false;

            Guid subscriptionId = this.UserManager.FindById(userId).SubscriptionId.Value;

            int managersCount = this.DbContext.RelayUserRoles.Where(r => r.Role.Name == iHandoverRoles.Relay.HeadLineManager
                                                                        || r.Role.Name == iHandoverRoles.Relay.LineManager
                                                                        || r.Role.Name == iHandoverRoles.Relay.SafetyManager)
                                                             .Select(r => r.User)
                                                             .Count(u => u.SubscriptionId == subscriptionId);

            return this.restrictions.MaxManagers <= managersCount;
        }

        public int MonthArchivesLimit()
        {
            return this.restrictions?.MaxMonthArchives ?? 0;
        }

        public bool UsersLimitExceeded(Guid userId)
        {
            if (this.restrictions == null) /*return true;*/
            {
                var ownerId = _authService.GetSubscriptionOwnerId();

                if (ownerId.HasValue)
                {
                    var restrictions = this.UserManager.FindById(ownerId.Value).Subscription?.Restrictions;

                    if (restrictions == null)
                    {
                        return true;
                    }
                    else
                    {
                        Guid subscriptionIdNew = this.UserManager.FindById(ownerId.Value).SubscriptionId.Value;

                        int usersCountNew = this.DbContext.RelayUserRoles.Where(r => r.Role.Name != iHandoverRoles.Relay.iHandoverAdmin).Where(u => !u.User.LockoutEndDateUtc.HasValue)
                                                                          .Select(r => r.User)
                                                                          .Count(u => u.SubscriptionId == subscriptionIdNew);

                        return restrictions.MaxUsers <= usersCountNew;
                    }
                }
            }

            Guid subscriptionId = this.UserManager.FindById(userId).SubscriptionId.Value;

            int usersCount = this.DbContext.RelayUserRoles.Where(r => r.Role.Name != iHandoverRoles.Relay.iHandoverAdmin).Where(u => !u.User.LockoutEndDateUtc.HasValue)
                                                              .Select(r => r.User)
                                                              .Count(u => u.SubscriptionId == subscriptionId);

            return this.restrictions.MaxUsers <= usersCount;

        }

        public bool UsersLimitExceededForActivate(Guid userId)
        {
            var restrictions = this.UserManager.FindById(userId).Subscription.Restrictions;

            if (restrictions == null) return false;

            Guid subscriptionId = this.UserManager.FindById(userId).SubscriptionId.Value;

            int usersCount = this.DbContext.RelayUserRoles.Where(r => r.Role.Name != iHandoverRoles.Relay.iHandoverAdmin).Where(u => !u.User.LockoutEndDateUtc.HasValue)
                                                              .Select(r => r.User)
                                                              .Count(u => u.SubscriptionId == subscriptionId);

            return restrictions.MaxUsers > usersCount;
        }


        public bool UsersLimit(Guid userId, int userCount)
        {
            if (this.restrictions == null) return false;

            Guid subscriptionId = this.UserManager.FindById(userId).SubscriptionId.Value;

            if (subscriptionId != null)
            {
                return userCount < this.restrictions.MaxUsers;
            }

            return false;
        }

        public bool IsLockUser(Guid userId)
        {
            return _authService.IsLockUser(userId);
        }

    }
}