﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Authorization.Auth.Managers;
using MomentumPlus.Relay.Authorization.Constants;
using MomentumPlus.Relay.Authorization.Domain;
using MomentumPlus.Relay.Authorization.Domain.Entities;
using MomentumPlus.Relay.Authorization.Services;
using MomentumPlus.Relay.Extensions;
using MomentumPlus.Relay.Helpers;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Roles;
using NLog;
using Stripe;

namespace MomentumPlus.Relay.Authorization.Adapter
{
    public class AuthService : IAuthService
    {
        private MultiTenancyContext DbContext => HttpContext.Current.GetOwinContext().Get<MultiTenancyContext>();
        private RelayRoleManager RoleManager => HttpContext.Current.GetOwinContext().Get<RelayRoleManager>();
        private RelayUserManager UserManager => HttpContext.Current.GetOwinContext().Get<RelayUserManager>();
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void SendMail(Guid userId, string userName, string urlUdtatePassword)
        {
            UserManager.SendEmail(userId, "Welcome to RelayWorks",
               " Hello, " + userName + "! <br />" +
               " You have been signed up to RelayWorks. <br />" +
               " To complete your account setup, please follow the link below and update your password:  <a href=\"" + urlUdtatePassword + "\">link</a>" +
               " <br /> Many Thanks, <br /> " +
               " RelayWorks Team");
        }

        public int? GetSubscriptionHandoverItemsLimitExceeded()
        {
            RelayUser user = UserManager.FindById(Guid.Parse(HttpContext.Current.User.Identity.GetUserId()));

            Guid? userSubscriptionId = user.SubscriptionId ?? AppSession.Current.CurrentSubscriptionId;

            if (userSubscriptionId.HasValue)
            {
                Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == userSubscriptionId);

                if (subscription.RestrictionType == RestrictionType.Basic)
                {
                    return 10;
                }

                if (subscription.RestrictionType == RestrictionType.Premium)
                {
                    return 20;
                }

                if (subscription.RestrictionType == RestrictionType.Ultimate)
                {
                    return null;
                }

                return subscription.Restrictions.MaxHandoverItems;
            }

            throw new ObjectNotFoundException("Current Subscription cannot be null");
        }

        public string GetEmailAdminOfSubscription()
        {
            RelayUser user = UserManager.FindById(Guid.Parse(HttpContext.Current.User.Identity.GetUserId()));

            Guid? userSubscriptionId = user.SubscriptionId ?? AppSession.Current.CurrentSubscriptionId;

            if (userSubscriptionId.HasValue)
            {
                Subscription subscription = DbContext.Subscriptions.AsNoTracking().FirstOrDefault(s => s.Id == userSubscriptionId);

                return subscription.Owner.Email;
            }

            return null;
        }

        public Guid? GetSubscriptionOwnerId()
        {
            RelayUser user = UserManager.FindById(Guid.Parse(HttpContext.Current.User.Identity.GetUserId()));

            Guid? userSubscriptionId = user.SubscriptionId ?? AppSession.Current.CurrentSubscriptionId;

            if (userSubscriptionId.HasValue)
            {
                Subscription subscription = DbContext.Subscriptions.AsNoTracking().FirstOrDefault(s => s.Id == userSubscriptionId);

                return subscription.OwnerId;
            }

            return null;
        }

        public void RegistrationRequest(Guid adminOfsubscriptionId, string nameAdminOfsubscription, string email)
        {
            UserManager.SendEmail(adminOfsubscriptionId, "Registration Request",
              " Hello, " + nameAdminOfsubscription + "! <br />" +
              " The user " + email + "has requested to be added to your subscription. <br />" +
              " <br /> Many Thanks, <br /> " +
              " RelayWorks Team");
        }


        public void SendAutoRenewSubscriptionMail(Guid userId, string emailOfSubscription)
        {
            var from = "noreply@ihandover.co";
            var pass = "Ball00n$%";

            SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(@from, pass),
                EnableSsl = true
            };

            var mail = new MailMessage(from, "info@ihandover.co")
            {
                Subject = "Subscription Cancellation",
                Body = "iHandover Team, <br />" +
                   "The subscription " + emailOfSubscription + " has turned the auto-renew billing off.<br />" +
                   "Kind Regards, <br /> " +
                   " RelayWorks",
                IsBodyHtml = true
            };

            client.Send(mail);
        }

        public string GetSecurityStampForUpdatePassword(Guid userId)
        {
            return UserManager.FindById(userId).SecurityStamp;
        }

        public bool AddUserToRole(Guid userId, string role)
        {
            bool result = true;
            RelayUser user = UserManager.FindById(userId);

            using (DbContextTransaction transaction = DbContext.Database.BeginTransaction())
            {
                result = UserManager.AddToRole(user.Id, role).Succeeded;
                transaction.Commit();
            }

            return result;
        }

        public bool RemoveUserFromRole(Guid userId, string role)
        {
            bool result = true;
            RelayUser user = UserManager.FindById(userId);

            using (DbContextTransaction transaction = DbContext.Database.BeginTransaction())
            {
                result = UserManager.RemoveFromRole(user.Id, role).Succeeded;
                transaction.Commit();
            }

            return result;
        }

        public IEnumerable<string> GetUserRoles(Guid userId)
        {
            return UserManager.GetRoles(userId);
        }


        public bool IsRole(Guid userId, string role)
        {
            return UserManager.IsInRole(userId, role);
        }


        public IEnumerable<string> GetAllRolesAccessedByUser(Guid userId)
        {
            return new[]
            {
                iHandoverRoles.Relay.Administrator,
                //iHandoverRoles.Relay.iHandoverAdmin,
                //iHandoverRoles.Relay.HeadLineManager,
                iHandoverRoles.Relay.LineManager,
                iHandoverRoles.Relay.SafetyManager,
                //iHandoverRoles.Relay.Contributor,
                iHandoverRoles.Relay.User,
                iHandoverRoles.Relay.ExecutiveUser
            };
        }

        public IEnumerable<Guid> GetUsersIdsByRole(string role, Guid requestOwnerId)
        {
            return RoleManager.FindByName(role).Users.Select(u => u.UserId);
        }

        public IEnumerable<Guid> GetUsersIdsByRoles(IEnumerable<string> roles, Guid requestOwnerId)
        {
            List<Guid> usersIds = new List<Guid>();

            foreach (string role in roles)
            {
                usersIds.AddRange(RoleManager.FindByName(role).Users.Select(u => u.UserId));
            }

            return usersIds.Distinct();
        }


        public bool CreateUser(string login, string password, bool needEmailConfirm = false)
        {
            RelayUser user = UserManager.FindById(Guid.Parse(HttpContext.Current.User.Identity.GetUserId()));

            Guid? userSubscriptionId = user.SubscriptionId ?? AppSession.Current.CurrentSubscriptionId;

            if (userSubscriptionId.HasValue)
            {
                return UserManager.Create(new RelayUser
                {
                    Id = Guid.NewGuid(),
                    UserName = login,
                    Email = login,
                    SubscriptionId = userSubscriptionId.Value,
                    EmailConfirmed = needEmailConfirm
                }, password).Succeeded;
            }

            throw new ObjectNotFoundException("Subscription cannot be null");
        }

        public bool RemoveUser(Guid userId, Guid initiatorId)
        {
            bool result = true;
            RelayUser user = UserManager.FindById(userId);
            IList<string> rolesForUser = UserManager.GetRoles(userId);

            using (DbContextTransaction transaction = DbContext.Database.BeginTransaction())
            {
                foreach (var login in user.Logins.ToList())
                {
                    UserLoginInfo userLoginInfo = new UserLoginInfo(login.LoginProvider, login.ProviderKey);
                    result = UserManager.RemoveLogin(login.UserId, userLoginInfo).Succeeded && result;
                }

                if (rolesForUser.Any())
                {
                    result = rolesForUser.Aggregate(result, (current, item) => UserManager.RemoveFromRole(user.Id, item).Succeeded && current);
                }

                result = UserManager.Delete(user).Succeeded && result;

                transaction.Commit();
            }

            return result;
        }

        public bool ChangeUserLogin(Guid userId, string newLogin)
        {
            RelayUser user = UserManager.FindById(userId);

            user.UserName = newLogin;
            user.Email = newLogin;

            return UserManager.Update(user).Succeeded;
        }

        public bool ChangeUserPassword(Guid userId, string password, string newPassword)
        {
            bool result;

            using (DbContextTransaction transaction = DbContext.Database.BeginTransaction())
            {
                result = UserManager.ChangePassword(userId, password, newPassword).Succeeded;
                transaction.Commit();
            }

            return result;
        }

        public bool ResetUserPassword(Guid userId, string newPassword)
        {
            bool result;

            using (DbContextTransaction transaction = DbContext.Database.BeginTransaction())
            {
                result = UserManager.RemovePassword(userId).Succeeded && UserManager.AddPassword(userId, newPassword).Succeeded;
                transaction.Commit();
            }

            return result;
        }

        public void UpdateLastLoginDateForUser(Guid userId)
        {
            var user = UserManager.FindById(userId);

            user.LastLoginDate = DateTime.Now;

            UserManager.Update(user);
        }

        public SubscriptionDetailsModel GetSubscriptionDetailsModel()
        {
            RelayUser user = UserManager.FindById(Guid.Parse(HttpContext.Current.User.Identity.GetUserId()));

            Guid? userSubscriptionId = user.SubscriptionId ?? AppSession.Current.CurrentSubscriptionId;

            if (userSubscriptionId.HasValue)
            {
                Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == userSubscriptionId);

                SubscriptionDetailsModel model = new SubscriptionDetailsModel
                {
                    PlanType = subscription.RestrictionType.GetEnumDescription(),
                    ManagerId = user.Id,
                    MaxManagers = subscription.Restrictions.MaxManagers,
                    MaxUsers = subscription.Restrictions.MaxUsers,
                    ManagerEmail = user.Email,
                    StartDate = subscription.CreatedDateUTC,
                    EndDate = subscription.SubscriptionEndDate
                };


                if (subscription.SubscriptionStatus == SubscriptionStatus.Active)
                {
                    var stripeCustomerService = new StripeCustomerService(StripeConstants.StripeApiKey);

                    if (subscription.StripeManagerId != null)
                    {
                        var stripeUser = stripeCustomerService.Get(subscription.StripeManagerId);

                        var stripeSubscription = stripeUser?.Subscriptions.FirstOrDefault();

                        if (stripeSubscription != null)
                        {
                            model.CancelAtPeriodEnd = stripeSubscription.CancelAtPeriodEnd;
                        }

                    }

                }

                return model;
            }

            throw new ObjectNotFoundException("Current Subscription cannot be null");

        }


        public void CancelSubscriptionAtPeriodEnd(bool autoRenewSubscription)
        {
            RelayUser user = UserManager.FindById(Guid.Parse(HttpContext.Current.User.Identity.GetUserId()));

            Guid? userSubscriptionId = user.SubscriptionId ?? AppSession.Current.CurrentSubscriptionId;

            if (userSubscriptionId.HasValue)
            {
                Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == userSubscriptionId);

                var stripeCustomerService = new StripeCustomerService(StripeConstants.StripeApiKey);

                if (subscription.StripeManagerId != null)
                {
                    var stripeUser = stripeCustomerService.Get(subscription.StripeManagerId);

                    var stripeSubscription = stripeUser?.Subscriptions.FirstOrDefault();

                    if (stripeSubscription != null)
                    {
                        var stripeSubscriptionService = new StripeSubscriptionService(StripeConstants.StripeApiKey);

                        stripeSubscriptionService.Update(stripeSubscription.Id, new StripeSubscriptionUpdateOptions() { CancelAtPeriodEnd = !autoRenewSubscription });
                    }

                }

            }
        }

        public void CancelSubscription(Guid ownerId)
        {
            RelayUser user = UserManager.FindById(ownerId);

            Guid? userSubscriptionId = user.SubscriptionId ?? AppSession.Current.CurrentSubscriptionId;

            if (userSubscriptionId.HasValue)
            {
                Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == userSubscriptionId);

                var stripeCustomerService = new StripeCustomerService(StripeConstants.StripeApiKey);

                if (subscription.StripeManagerId != null)
                {
                    var stripeUser = stripeCustomerService.Get(subscription.StripeManagerId);

                    var stripeSubscription = stripeUser?.Subscriptions.FirstOrDefault();

                    if (stripeSubscription != null)
                    {
                        var stripeSubscriptionService = new StripeSubscriptionService(StripeConstants.StripeApiKey);
                        stripeSubscriptionService.Cancel(stripeSubscription.Id);
                    }
                }
            }
        }

        public Guid GetUserIdBySubscriptionId(Guid? subscriptionId)
        {
            Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == subscriptionId);
            return subscription.OwnerId;
        }



        public UpgradeSubscriptionViewModel GetUpgradeSubscriptionViewModel()
        {
            RelayUser user = UserManager.FindById(Guid.Parse(HttpContext.Current.User.Identity.GetUserId()));

            Guid? subscriptionId = user.SubscriptionId ?? AppSession.Current.CurrentSubscriptionId;


            Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == subscriptionId);


            if (subscriptionId.HasValue && subscription != null)
            {
                var stripeCustomerService = new StripeCustomerService(StripeConstants.StripeApiKey);

                StripeCustomer stripeUser = null;

                StripePlan stripePlan = null;

                if (subscription.StripeManagerId != null)
                {
                    stripeUser = stripeCustomerService.Get(subscription.StripeManagerId);

                    var stripeSubscription = stripeUser?.Subscriptions.FirstOrDefault();

                    stripePlan = stripeSubscription?.StripePlan;
                }

                SubscriptionPricesConstants priceConstants = DbContext.SubscriptionPricesConstants.FirstOrDefault();


                UpgradeSubscriptionViewModel model = new UpgradeSubscriptionViewModel
                {
                    NumberOfusers = subscription.Restrictions.MaxUsers,
                    IsTrial = subscription.SubscriptionStatus == SubscriptionStatus.Trialing,
                    SubscriptionLevelType = (SubscriptionRestrictionType)subscription.RestrictionType,
                    CardNumberLast4 = stripeUser?.Sources.Data[0]?.Card?.Last4,
                    CardBrand = stripeUser?.Sources.Data[0]?.Card?.Brand,
                    NewPrice = subscription.SubscriptionStatus == SubscriptionStatus.Trialing ? subscription.Restrictions.MaxUsers * priceConstants.UserPriceForBasicType : 0,
                    CurrentPrice = stripePlan?.Amount / 100 ?? 0,
                    SubscriptionLevelPriceBasic = 12 * subscription.Restrictions.MaxUsers,
                    SubscriptionLevelPricePremium = 24 * subscription.Restrictions.MaxUsers,
                    SubscriptionLevelPriceUltimate = 30 * subscription.Restrictions.MaxUsers,
                    UserPriceForBasicType = 12,
                    UserPriceForPremiumType = 24,
                    UserPriceForUltimateType = 30,
                    StripePublicKey = "pk_test_KKAq5LvlPGkxSkAoc35hUt5Z",
                    Email = subscription.Owner.Email,
                    SubscriptionId = subscription.Id,
                    Countries = GetAllCountries()
                };

                return model;
            }

            throw new ObjectNotFoundException("Current Subscription cannot be null");
        }


        private string[] GetAllCountries()
        {
            Dictionary<string, string> objDic = new Dictionary<string, string>();

            foreach (CultureInfo ObjCultureInfo in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                RegionInfo objRegionInfo = new RegionInfo(ObjCultureInfo.Name);
                if (!objDic.ContainsKey(objRegionInfo.EnglishName))
                {
                    objDic.Add(objRegionInfo.EnglishName, objRegionInfo.TwoLetterISORegionName.ToLower());
                }
            }

            var obj = objDic.OrderBy(p => p.Key);
            var y = obj.Select(t => t.Key);
            return y.ToArray();
        }



        public bool SubscriptionMaxUserCanChange(int newUserNumber)
        {
            RelayUser user = UserManager.FindById(Guid.Parse(HttpContext.Current.User.Identity.GetUserId()));

            Guid? subscriptionId = user.SubscriptionId ?? AppSession.Current.CurrentSubscriptionId;

            Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == subscriptionId);

            if (subscriptionId.HasValue && subscription != null)
            {
                //return subscription.Users.Count(u => !u.LockoutEndDateUtc.HasValue) >= newUserNumber;

                var subscriptionUsersCount = subscription.Users.Count;

                return subscriptionUsersCount <= newUserNumber;

            }

            return false;
        }



        public bool UpgradeSubscription(UpgradeSubscriptionViewModel model)
        {
            RelayUser user = UserManager.FindById(Guid.Parse(HttpContext.Current.User.Identity.GetUserId()));

            Guid? subscriptionId = user.SubscriptionId ?? AppSession.Current.CurrentSubscriptionId;

            Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == subscriptionId);

            if (subscriptionId.HasValue && subscription != null) 
            {
                if (subscription.SubscriptionStatus != SubscriptionStatus.Canceled || subscription.SubscriptionStatus != SubscriptionStatus.TrialEnd)
                {
                    var stripeCustomerService = new StripeCustomerService(StripeConstants.StripeApiKey);

                    StripeCustomer stripeUser = null;

                    StripePlan stripePlan = null;

                    if (subscription.StripeManagerId != null)
                    {
                        stripeUser = stripeCustomerService.Get(subscription.StripeManagerId);

                        var stripeSubscription = stripeUser?.Subscriptions.FirstOrDefault();

                        stripePlan = stripeSubscription?.StripePlan;

                        if (stripeSubscription != null && stripePlan != null)
                        {
                            var planService = new StripePlanService(StripeConstants.StripeApiKey);

                            try
                            {
                                planService.Delete(subscription.StripePlanId);
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, " - DELETE STRIPE");
                            }
                            

                            var planOptions = new StripePlanCreateOptions
                            {
                                Id = Guid.NewGuid().ToString(),
                                Amount = CalculateNewPrice(model),
                                Currency = "gbp",
                                Interval = "month",
                                Nickname = "Plan for RelayWorks user: " + subscription.Owner.Email,
                                Product = new StripePlanProductCreateOptions
                                {
                                    Id = Guid.NewGuid().ToString(),
                                    Name = "Plan for RelayWorks user: " + subscription.Owner.Email
                                }
                            };

                            StripePlan newPlan = planService.Create(planOptions);

                            var subscriptionOptions = new StripeSubscriptionUpdateOptions()
                            {
                                PlanId = newPlan.Id,
                                Prorate = true
                            };

                            // var subscriptionService = new StripeSubscriptionService(StripeConstants.StripeApiKey);
                            // subscriptionService.Update(stripeSubscription.Id, subscriptionOptions);

                            stripeSubscription.StripePlan.Id = newPlan.Id;
                            subscription.StripePlanId = newPlan.Id;
                            stripePlan.Id = newPlan.Id;

                             SubscriptionService service = new SubscriptionService();
                            service.UpdatePaymentSubscription(subscriptionId.Value, newPlan);
                        }
                    }

                    subscription.Restrictions.MaxUsers = model.NumberOfusers;
                    subscription.RestrictionType = (RestrictionType)model.SubscriptionLevelType;
                    PopulateSubscriptionRestrictions(subscription, subscription.RestrictionType);
                    DbContext.SaveChanges();
                }
            }

            return false;
        }


        private int CalculateNewPrice(UpgradeSubscriptionViewModel model)
        {
            SubscriptionPricesConstants priceConstants = DbContext.SubscriptionPricesConstants.FirstOrDefault();

            int amount = 0;

            switch (model.SubscriptionLevelType)
            {
                case SubscriptionRestrictionType.Basic:
                    amount = (int)(priceConstants.UserPriceForBasicType * model.NumberOfusers);
                    break;
                case SubscriptionRestrictionType.Premium:
                    amount = (int)(priceConstants.UserPriceForPremiumType * model.NumberOfusers);
                    break;
                case SubscriptionRestrictionType.Ultimate:
                    amount = (int)(priceConstants.UserPriceForUltimateType * model.NumberOfusers);

                    break;
            }

            return amount * 100;
        }


        private void PopulateSubscriptionRestrictions(Subscription subscription, RestrictionType restrictionType)
        {
            switch (restrictionType)
            {
                case RestrictionType.Basic:
                    subscription.Restrictions.MaxCarryForwards = 0;
                    subscription.Restrictions.MaxEmailShares = 0;
                    subscription.Restrictions.MaxMonthArchives = 0;
                    subscription.Restrictions.MaxHandoverItems = 10;
                    subscription.Restrictions.MaxHandoverTasks = 3;
                    subscription.Restrictions.CompanyBranding = false;
                    break;
                case RestrictionType.Premium:
                    subscription.Restrictions.MaxCarryForwards = 3;
                    subscription.Restrictions.MaxEmailShares = 3;
                    subscription.Restrictions.MaxMonthArchives = 6;
                    subscription.Restrictions.MaxHandoverItems = 20;
                    subscription.Restrictions.MaxHandoverTasks = 10;
                    subscription.Restrictions.CompanyBranding = true;
                    break;
                case RestrictionType.Ultimate:
                    subscription.Restrictions.MaxCarryForwards = null;
                    subscription.Restrictions.MaxEmailShares = null;
                    subscription.Restrictions.MaxMonthArchives = null;
                    subscription.Restrictions.MaxHandoverItems = null;
                    subscription.Restrictions.MaxHandoverTasks = null;
                    subscription.Restrictions.CompanyBranding = true;
                    break;
            }
        }


        public Guid GetUserIdByLogin(string login)
        {
            return UserManager.FindByName(login).Id;
        }

        public DateTime? GetLastLoginDateForUser(Guid userId)
        {
            return UserManager.FindById(userId).LastLoginDate;
        }

        public void ReactivateUser(Guid userId, bool status)
        {
            UserManager.SetLockoutEnabled(userId, status);
            DateTimeOffset lockEndDate = status ? DateTimeOffset.MaxValue : DateTimeOffset.Now;
            UserManager.SetLockoutEndDate(userId, lockEndDate);
        }

        public bool IsLockUser(Guid userId)
        {
            return UserManager.IsLockedOut(userId);
        }

        public Guid? GetUserSubscriptionId(Guid userId)
        {
            RelayUser user = UserManager.FindById(userId);

            Guid? subscriptionId = null;

            if (user != null)
            {
                Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == user.SubscriptionId.Value);

                subscriptionId = subscription?.Id;
            }

            return subscriptionId;
        }


        public bool IsTrialSubscription(Guid userId)
        {
            Guid? subscriptionId = GetUserSubscriptionId(userId);
            Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == subscriptionId);

            if (subscription != null && subscription.SubscriptionStatus == SubscriptionStatus.Trialing)
            {
                return true;
            }

            return false;
        }

        public int? NumberOfRemainingTrialDays(Guid userId)
        {
            Guid? subscriptionId = GetUserSubscriptionId(userId);
            Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == subscriptionId);

            if (subscription != null)
            {
                if (subscription.SubscriptionStatus == SubscriptionStatus.Trialing && subscription.SubscriptionEndDate.HasValue)
                {
                    var today = DateTime.UtcNow;
                    if (subscription.CreatedDateUTC.Date == today.Date)
                    {
                        return (subscription.SubscriptionEndDate.Value - subscription.CreatedDateUTC).Days;
                    }
                    else
                    {
                        return (int)(subscription.SubscriptionEndDate.Value - today).TotalDays;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public bool ContributorEnable(Guid userId)
        {
            Guid? subscriptionId = GetUserSubscriptionId(userId);
            Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == subscriptionId);

            if (subscription != null)
            {
                if (subscription.ContributorEnabled)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        public RestrictionType? GetSubscriptionType(Guid userId)
        {
            RelayUser user = UserManager.FindById(userId);

            Guid? userSubscriptionId = user.SubscriptionId ?? AppSession.Current.CurrentSubscriptionId;
            Subscription subscription = DbContext.Subscriptions.AsNoTracking().FirstOrDefault(s => s.Id == userSubscriptionId);

            return subscription.RestrictionType;
        }


        public string GetTimeZoneFromSubscription(Guid userId)
        {
            RelayUser user = UserManager.FindById(userId);
            Guid? userSubscriptionId = user.SubscriptionId ?? AppSession.Current.CurrentSubscriptionId;
            Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == userSubscriptionId);

            if (subscription != null)
            {
                return subscription.TimeZone;
            }
            else
            {
                return null;
            }
        }

        public bool GetAccessToPageUpgrade(Guid userId)
        {
            RelayUser user = UserManager.FindById(userId);
            var result = user.Roles.Where(u => u.Role.Name == iHandoverRoles.Relay.Administrator || u.Role.Name == iHandoverRoles.Relay.iHandoverAdmin);

            if (result.Count() > 0)
            {
                return true;
            }

            return false;
        }


        public void AddSector(SectorViewModule model)
        {
            SubscriptionService service = new SubscriptionService();

            AdminSectors adminSectors = new AdminSectors
            {
                Id = model.Id.Value,
                Name = model.Name
            };

            service.SaveSectors(adminSectors);
        }

        public void AddTemplate(TemplateViewModel model)
        {
            SubscriptionService service = new SubscriptionService();

            AdminTemplates adminTemplate = new AdminTemplates
            {
                Id = model.Id.Value,
                Name = model.Name,
                AdminSectorsId = model.SelectedSectorsId
            };

            service.SaveTemplates(adminTemplate);
        }

        public void AddExistTemplate(Template template)
        {
            SubscriptionService service = new SubscriptionService();

            AdminTemplates adminTemplate = new AdminTemplates
            {
                Id = template.Id,
                Name = template.Name
            };

            service.SaveTemplates(adminTemplate);
        }

        public IEnumerable<SelectListItem> GetAdminSectors()
        {
            SubscriptionService service = new SubscriptionService();

            var sectors =  service.GetAdminSectors();

            return sectors.OrderBy(t => t.Name).Select(t => new SelectListItem
            {
                Text = t.Name,
                Value = t.Id.ToString()
            });
        }

        public IEnumerable<SelectListItem> GetAdminTemplates(Guid? sectorId)
        {
            SubscriptionService service = new SubscriptionService();

           var templates = service.GetAdminTemplates(sectorId);

            return templates.OrderBy(t => t.Name).Select(t => new SelectListItem
            {
                Text = t.Name,
                Value = t.Id.ToString()
            });
        }

        public Guid? GetSectorByTemplateId(Guid? templateId)
        {
            SubscriptionService service = new SubscriptionService();

            if (templateId.HasValue)
            {
                return service.GetSectorByTemplateId(templateId.Value);
            }

            return null;
        }

        public IEnumerable<SelectListItem> GetAdminTopicGroupsByType(Domain.Entities.TypeOfModule moduleType)
        {
            SubscriptionService service = new SubscriptionService();

            var topicGroups = service.GetAdminTopicGroupsByType(moduleType);

            return topicGroups.OrderBy(t => t.Name).Select(t => new SelectListItem
            {
                Text = t.Name,
                Value = t.Id.ToString()
            });
        }

        public IEnumerable<SelectListItem> GetAdminTopicsByType(Domain.Entities.TypeOfModule moduleType)
        {
            SubscriptionService service = new SubscriptionService();

            var topicGroups = service.GetAdminTopicsByType(moduleType);

            return topicGroups.OrderBy(t => t.Name).Select(t => new SelectListItem
            {
                Text = t.Name,
                Value = t.Id.ToString()
            });
        }

        public AdminTemplates GetAdminTemplateById(Guid templateId)
        {
            SubscriptionService service = new SubscriptionService();
            return service.GetAdminTemplateById(templateId);
        }

        public List<RelayUser> GetAdminsOfSunscription()
        {
            return DbContext.Subscriptions.Select(t => t.Owner).Where(t => t.SubscriptionId.HasValue).ToList();
        }

        public Guid AddTopicGroup(rTopicGroupViewModel model, Domain.Entities.TypeOfModule type)
        {
            SubscriptionService service = new SubscriptionService();

            var topicGroup = new AdminTemplateTopicGroups
            {
                Description = model.Description,
                Id = Guid.NewGuid(),
                Enabled = true,
                ModuleId = model.ModuleId,
                RelationId = model.RelationId,
                ParentTopicGroupId = model.ParentTopicGroupId,
                Name = model.Name,
                Type = type,
                TemplateId = model.TemplateId
            };

            service.SaveAdminTopicGroups(topicGroup);

            return topicGroup.Id;
        }

        public Guid AddTopic(rTopicViewModel model, Authorization.Domain.Entities.TypeOfModule type)
        {
            SubscriptionService service = new SubscriptionService();

            model.Metrics = model.Metrics.HasValue ? model.Metrics.Value : false;

            var topic = new AdminTemplateTopics
            {
                Enabled = true,
                Description = model.Description,
                IsExpandPlannedActualField = model.Metrics,
                Id = Guid.NewGuid(),
                Name = model.Name,
                TopicGroupId = model.TopicGroupId.Value,
                ParentTopicId = model.ParentTopicId,
                Type = type,
                UnitsSelectedType = model.UnitsSelectedType
            };

            if (topic.ParentTopic != null)
            {
                topic.ParentTopic.IsExpandPlannedActualField = topic.IsExpandPlannedActualField;
                topic.ParentTopic.UnitsSelectedType = topic.UnitsSelectedType;
            }

            service.SaveAdminTopics(topic);

            return topic.Id;
        }

        public List<AdminTemplateTopicGroups> GetAdminTopicGroups(Authorization.Domain.Entities.TypeOfModule moduleType, Guid templateId)
        {
            SubscriptionService service = new SubscriptionService();

            var topicGroups = service.GetAdminTopicGroups(moduleType, templateId);

            return topicGroups;
        }

        public AdminTemplateTopicGroups GetGlobalTopicGroup(Guid topicGroupId)
        {
            SubscriptionService service = new SubscriptionService();

            var topicGroup = service.GetAdminTopicGroup(topicGroupId);

            return topicGroup;
        }


        public void UpdateGlobalTopicGroup(Guid topicGroupId, bool status)
        {
            SubscriptionService service = new SubscriptionService();

            service.UpdateAdminTopicGroup(topicGroupId, status);
        }

        public void UpdateGlobalTopic(Guid topicGroupId, Guid topicId, bool status)
        {
            SubscriptionService service = new SubscriptionService();

            service.UpdateAdminTopic(topicGroupId, topicId, status);
        }

        public AdminTemplateTopics GetGlobalTopic(Guid topicGroupId, Guid topicId)
        {
            SubscriptionService service = new SubscriptionService();

            var topic = service.GetAdminTopic(topicGroupId, topicId);

            return topic;
        }

        public void CopyRelayRotations(Rotation rotation)
        {
            SubscriptionService service = new SubscriptionService(); 

            service.CopyToRelayRotation(rotation);
        }

        public void CopyRelayAdminSettings(AdminSettings adminSettings, Guid rotationId)
        {
            SubscriptionService service = new SubscriptionService();

            service.CopyToRelaySettings(adminSettings, rotationId);
        }

        public void CopyRelayShifts(Shift shift)
        {
            SubscriptionService service = new SubscriptionService();

            service.CopyToRelayShift(shift);
        }

        public void CopyRelayRotationTasks(RotationTask rotationTask, Guid rotationId)
        {
            SubscriptionService service = new SubscriptionService();

            service.CopyToRelayRotationTask(rotationTask, rotationId);
        }


        public RotationModule GetRelayRotationModule(Core.Models.TypeOfModule type, Guid rotationId, Core.Models.TypeOfModuleSource sourceType)
        {
            SubscriptionService service = new SubscriptionService();

            var relayModule = service.GetRotationModule((Domain.Entities.TypeOfModule)type, rotationId, (Domain.Entities.TypeOfModuleSource)sourceType);

            if (relayModule != null)
            {
                return new RotationModule
                {
                    CreatedUtc = relayModule.CreatedUtc,
                    DeletedUtc = relayModule.DeletedUtc,
                    Enabled = relayModule.Enabled,
                    Id = relayModule.Id,
                    ModifiedUtc = relayModule.ModifiedUtc,
                    RotationId = relayModule.RotationId,
                    SourceType = (Core.Models.TypeOfModuleSource)relayModule.SourceType,
                    Type = (Core.Models.TypeOfModule)relayModule.Type
                };
            }

            return null;
        }

        public RotationModule GetShiftRelayRotationModule(Core.Models.TypeOfModule type, Guid shiftId, Core.Models.TypeOfModuleSource sourceType)
        {
            SubscriptionService service = new SubscriptionService();

            var relayModule = service.GetShiftRotationModule((Domain.Entities.TypeOfModule)type, shiftId, (Domain.Entities.TypeOfModuleSource)sourceType);

            return new RotationModule
            {
                CreatedUtc = relayModule.CreatedUtc,
                DeletedUtc = relayModule.DeletedUtc,
                Enabled = relayModule.Enabled,
                Id = relayModule.ParentModuleId.HasValue ? relayModule.ParentModuleId.Value : relayModule.Id,
                ModifiedUtc = relayModule.ModifiedUtc,
                ShiftId = shiftId,
                SourceType = (Core.Models.TypeOfModuleSource)relayModule.SourceType,
                Type = (Core.Models.TypeOfModule)relayModule.Type
            };
        }

        public List<RotationTopicGroup> GetRelayRotationTopicGroups(Guid rotationId, Guid moduleId, bool IsShift = false)
        {
            SubscriptionService service = new SubscriptionService();

            List<RelayRotationTopicGroup> relayRotationTopicGroups = new List<RelayRotationTopicGroup>();

            if (IsShift)
            {
                relayRotationTopicGroups = service.GetShiftRotationTopicGroups(rotationId, moduleId);
            }
            else
            {
                relayRotationTopicGroups = service.GetRotationTopicGroups(rotationId, moduleId);
            }

            List<RotationTopicGroup> rotationTopicGroups = new List<RotationTopicGroup>();

            foreach (var topicGroup in relayRotationTopicGroups)
            {
                rotationTopicGroups.Add(new RotationTopicGroup
                {
                    CreatedUtc = topicGroup.CreatedUtc,
                    DeletedUtc = topicGroup.DeletedUtc,
                    Description = topicGroup.Description,
                    Enabled = topicGroup.Enabled,
                    Id = topicGroup.Id,
                    ModifiedUtc = topicGroup.ModifiedUtc,
                    Name = topicGroup.Name,
                    ParentRotationTopicGroupId = topicGroup.ParentRotationTopicGroupId,
                    RelationId = topicGroup.RelationId,
                    RotationModuleId = topicGroup.RotationModuleId

                });
            }

            return rotationTopicGroups;
        }

        public string GetNameRelayRotationTopicGroup(Guid topicGroupId)
        {
            SubscriptionService service = new SubscriptionService();

            return service.GetRelayRotationTopicGroup(topicGroupId).Name;
        }

        public string GetDefaultBackToBackName(Guid rotationId)
        {
            SubscriptionService service = new SubscriptionService();

            return service.GetRelayRotation(rotationId).DefaultBackToBackName;
        }

        public string GetShiftDefaultBackToBackName(Guid shiftId)
        {
            SubscriptionService service = new SubscriptionService();

            return service.GetRelayShift(shiftId).DefaultBackToBackName;
        }

        public string GetShiftGetOwnerRotationName(Guid shiftId)
        {
            SubscriptionService service = new SubscriptionService();

            return service.GetRelayShift(shiftId).RotationOwnerName;
        }

        public string GetOwnerRotationName(Guid rotationId)
        {
            SubscriptionService service = new SubscriptionService();

            return service.GetRelayRotation(rotationId).RotationOwnerName;
        }

        public string GetCompanyNameFromRotation(Guid rotationId)
        {
            SubscriptionService service = new SubscriptionService();

            return service.GetRelayRotation(rotationId).CompanyName;
        }

        public string GetCompanyNameFromShift(Guid shiftId)
        {
            SubscriptionService service = new SubscriptionService();

            return service.GetRelayShift(shiftId).CompanyName;
        }

        public string GetShiftOwnerEmailRotation(Guid shiftId)
        {
            SubscriptionService service = new SubscriptionService();

            return service.GetRelayShift(shiftId).RotationOwnerEmail;
        }

        public string GetOwnerEmailRotation(Guid rotationId)
        {
            SubscriptionService service = new SubscriptionService();

            return service.GetRelayRotation(rotationId).RotationOwnerEmail;
        }

        public string GetRecipients(Guid topicId)
        {
            SubscriptionService service = new SubscriptionService();

            var relayRotationTopic = service.GetRotationTopic(topicId);

            return relayRotationTopic != null ? relayRotationTopic.RecipientsName : null;
        }

        public List<RotationTopic> GetRelayRotationTopics(Guid topicGroupId)
        {
            SubscriptionService service = new SubscriptionService();

            var relayRotationTopics = service.GetRotationTopics(topicGroupId);

            List<RotationTopic> rotationTopics = new List<RotationTopic>();

            foreach (var topic in relayRotationTopics)
            {
                rotationTopics.Add(new RotationTopic
                {
                    CreatedUtc = topic.CreatedUtc,
                    Actual = topic.Actual,
                    CreatorId = topic.CreatorId,
                    Id = topic.Id,
                    IsNullReport = topic.IsNullReport,
                    IsPinned = topic.IsPinned,
                    Description = topic.Description,
                    DeletedUtc = topic.DeletedUtc,
                    Enabled = topic.Enabled,
                    ModifiedUtc = topic.ModifiedUtc,
                    Name = topic.Name,
                    Planned = topic.Planned,
                    RotationTopicGroupId = topic.RotationTopicGroupId,
                    SearchTags = topic.SearchTags,
                    UnitsSelectedType = topic.UnitsSelectedType,
                    Variance = topic.Variance
                });
            }

            return rotationTopics;
        }

        public Shift GetShift(Guid shiftId)
        {
            SubscriptionService service = new SubscriptionService();

            var relayShift = service.GetRelayShift(shiftId);

            var shift = new Shift
            {
                CreatedUtc = relayShift.CreatedUtc,
                BreakMinutes = relayShift.BreakMinutes,
                DeletedUtc = relayShift.DeletedUtc,
                Id = relayShift.Id,
                ModifiedUtc = relayShift.ModifiedUtc,
                RepeatTimes = relayShift.RepeatTimes,
                RotationId = relayShift.RotationId,
                ShiftRecipientId = relayShift.ShiftRecipientId,
                StartDateTime = relayShift.StartDateTime,
                State = (Core.Models.ShiftState)relayShift.State,
                WorkMinutes = relayShift.WorkMinutes,
                AdminSubscriptionId = relayShift.AdminSubscriptionId,
                SubscriptionId = relayShift.SubscriptionId
            };

            return shift;
        }

        public List<string> GetShiftRecipientsShareReport(Guid shiftId)
        {
            SubscriptionService service = new SubscriptionService();

            var relayShift = service.GetRelayShift(shiftId);

            return relayShift.RecipientsName.Split(',').Select(t => t).ToList();
        }

        public List<string> GetRecipientsShareReport(Guid rotationId)
        {
            SubscriptionService service = new SubscriptionService();

            var relayRotation = service.GetRelayRotation(rotationId);

            return relayRotation.RecipientsName.Split(',').Select(t => t).ToList();
        }

        public File GetRelayFileLogo(Guid rotationId)
        {
            SubscriptionService service = new SubscriptionService();

            var fileRelayLogo = service.GetRelayLogoFile(rotationId);

            if (fileRelayLogo != null)
            {
                return new File
                {
                    BinaryData = fileRelayLogo.BinaryData,
                    Id = fileRelayLogo.Id,
                    ContentType = fileRelayLogo.ContentType,
                    FileType = fileRelayLogo.FileType,
                    Title = fileRelayLogo.FileType
                };
            }
            else
            {
                return null;
            }
        }

        public Rotation GetRotation(Guid rotationId)
        {
           SubscriptionService service = new SubscriptionService();

           var relayRotation = service.GetRelayRotation(rotationId);

            var rotation = new Rotation
            {
                ConfirmDate = relayRotation.ConfirmDate,
                CreatedUtc = relayRotation.CreatedUtc,
                DayOff = relayRotation.DayOff,
                DayOn = relayRotation.DayOn,
                DefaultBackToBackId = relayRotation.DefaultBackToBackId,
                DeletedUtc = relayRotation.DeletedUtc,
                Id = relayRotation.Id,
                LineManagerId = relayRotation.LineManagerId,
                ModifiedUtc = relayRotation.ModifiedUtc,
                RepeatTimes = relayRotation.RepeatTimes,
                RotationOwnerId = relayRotation.RotationOwnerId,
                RotationType = (MomentumPlus.Core.Models.RotationType)relayRotation.RotationType,
                StartDate = relayRotation.StartDate,
                State = (RotationState)relayRotation.State,
                AdminSubscriptionId = relayRotation.AdminSubscriptionId,
                SubscriptionId = relayRotation.SubscriptionId
            };

            return rotation;
        }

        public List<RotationTask> GetRotationTask(Guid rotationId)
        {
            SubscriptionService service = new SubscriptionService();

            var relayTasks = service.GetRelayRotationTask(rotationId);

            List<RotationTask> rotationTasks = new List<RotationTask>();

            foreach (var relayTask in relayTasks)
            {
                rotationTasks.Add(new RotationTask
                {
                    Id = relayTask.Id,
                    Name = relayTask.Name,
                    Priority = (PriorityOfTask)relayTask.PriorityOfRelayTask,
                    Deadline = relayTask.Deadline,
                    Description = relayTask.Description,
                    IsNullReport = relayTask.IsNullReport,
                    IsPinned = relayTask.IsPinned
                });
            }

            return rotationTasks;
        }

        public Guid? GetIdAdminOfSubscription()
        {
            Guid? userSubscriptionId = AppSession.Current.CurrentSubscriptionId;
            Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == userSubscriptionId);

            if (subscription != null)
            {
                return subscription.OwnerId;
            }
            else
            {
                return null;
            }
        }

        public string GetAdminOfSubscription(Guid subscriptionId)
        {
            try
            {
                Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == subscriptionId);

                if (subscription != null)
                {
                    return subscription.Owner.Email;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                SubscriptionService service = new SubscriptionService();
                return service.GetAdminEmailFromRelayRotation(subscriptionId);
            }
        }

        public Guid GetAdminIdOfSubscription(Guid subscriptionId)
        {
            try
            {
                Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == subscriptionId);
                return subscription.Owner.Id;
            }
            catch (Exception ex)
            {
                SubscriptionService service = new SubscriptionService();
                return service.GetAdminIdFromRelayRotation(subscriptionId);
            }
        }


        public Guid? GetSubscriptionId()
        {
            Guid? userSubscriptionId = AppSession.Current.CurrentSubscriptionId;
            Subscription subscription = DbContext.Subscriptions.FirstOrDefault(s => s.Id == userSubscriptionId);

            if (subscription != null)
            {
                return subscription.Id;
            }
            else
            {
                return null;
            }
        }

        public void SetNewSubscriptionPeriod(Guid subscriptionId, DateTime newStartDate, DateTime newEndDate, SubscriptionStatus subscriptionStatus)
        {
            SubscriptionService service = new SubscriptionService();
            service.SetNewPeriod(subscriptionId, newStartDate, newEndDate, subscriptionStatus);
        }

        public SubscriptionNewPeriodViewModel GetSubscriptionNewPeriodViewModel(Guid subscriptionId)
        {
            SubscriptionService service = new SubscriptionService();
            var subscription = service.GetSubscription(subscriptionId);

            return new SubscriptionNewPeriodViewModel
            {
                SubscriptionId = subscriptionId,
                SubscriptionStatus = subscription.SubscriptionStatus,
                SubscriptionOwnerEmail = subscription.Owner.Email,
                StartDate = subscription.CreatedDateUTC,
                EndDate = subscription.SubscriptionEndDate.HasValue ? subscription.SubscriptionEndDate.Value : DateTime.UtcNow
            };
        }
    }
}