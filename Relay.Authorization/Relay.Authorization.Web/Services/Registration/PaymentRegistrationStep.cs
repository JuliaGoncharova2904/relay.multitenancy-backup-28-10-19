﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using MomentumPlus.Relay.Authorization.Constants;
using MomentumPlus.Relay.Authorization.Domain;
using MomentumPlus.Relay.Authorization.Domain.Entities;
using MomentumPlus.Relay.Authorization.Models;
using MomentumPlus.Relay.Models;
using Stripe;
using RegistrationViewModel = MomentumPlus.Relay.Authorization.Models.RegistrationViewModel;

namespace MomentumPlus.Relay.Authorization.Services.Registration
{
    class PaymentRegistrationStep : RegistrationStep
    {
        private readonly GenericUnitOfWork _unitOfWork;

        private readonly Mediator _mediator;

        private readonly StripePlanService _stripePlanService;

        private readonly StripeCustomerService _stripeCustomerService;

        public PaymentRegistrationStep(Mediator mediator) : base(mediator)
        {
            this._unitOfWork = new GenericUnitOfWork();
            this._mediator = mediator;

            this._stripePlanService = new StripePlanService(StripeConstants.StripeApiKey);
            this._stripeCustomerService = new StripeCustomerService(StripeConstants.StripeApiKey);
        }

        public override void Rollback()
        {
            if (!String.IsNullOrEmpty(_mediator.CreatedStripePlanId))
            {
                _stripePlanService.Delete(_mediator.CreatedStripePlanId);
            }

            if (!String.IsNullOrEmpty(_mediator.CreatedStripeUserId))
            {
                _stripeCustomerService.Delete(_mediator.CreatedStripeUserId);
            }
        }

        public override void Update()
        {
        }


        #region Registration

        public override void Run(RegistrationViewModel registrationData, ModelStateDictionary modelState)
        {
            try
            {
                _mediator.Amount = CalculatePrice(registrationData);
                _mediator.CreatedStripePlanId = CreateStripePlan(registrationData);
                _mediator.CreatedStripeUserId = CreateStripeUser(registrationData, _mediator.CreatedStripePlanId);
            }
            catch (StripeException exception)
            {
                switch (exception.StripeError.Code)
                {
                    case "invalid_number":
                        modelState.AddModelError("CardNumber", "");
                        break;
                    case "invalid_expiry_month":
                        modelState.AddModelError("ExpDate", "");
                        break;
                    case "invalid_expiry_year":
                        modelState.AddModelError("ExpDate", "");
                        break;
                    case "invalid_cvc":
                        modelState.AddModelError("CVC", "");
                        break;
                    case "invalid_swipe_data":
                        goto default;
                    case "incorrect_number":
                        modelState.AddModelError("CardNumber", "");
                        break;
                    case "expired_card":
                        modelState.AddModelError("CardNumber", "");
                        break;
                    case "incorrect_cvc":
                        modelState.AddModelError("CVC", "");
                        break;
                    case "incorrect_zip":
                        goto default;
                    case "card_declined":
                        modelState.AddModelError("CardNumber", "");
                        break;
                    case "missing":
                        modelState.AddModelError("CardNumber", "");
                        break;
                    case "processing_error":
                        goto default;
                    default:
                        break;
                }

                modelState.AddModelError("", @"Payment Error: " + exception.StripeError.Message);
                throw new RegistrationException(@"Payment Error: " + exception.StripeError.Message);
            }
            catch (Exception exception)
            {
                modelState.AddModelError("", @"Error of implementation Payment: " + exception.Message);
                throw new RegistrationException(@"Error of implementation Payment: " + exception.Message);
            }
        }

        public override void Run(UpgradeSubscriptionViewModel upgradeData, ModelStateDictionary modelState)
        {
            try
            {
                _mediator.Amount = CalculatePrice(upgradeData);
                _mediator.CreatedStripePlanId = CreateStripePlan(upgradeData);
                _mediator.CreatedStripeUserId = CreateStripeUser(upgradeData, _mediator.CreatedStripePlanId);
            }
            catch (StripeException exception)
            {
                switch (exception.StripeError.Code)
                {
                    case "invalid_number":
                        modelState.AddModelError("CardNumber", "");
                        break;
                    case "invalid_expiry_month":
                        modelState.AddModelError("ExpDate", "");
                        break;
                    case "invalid_expiry_year":
                        modelState.AddModelError("ExpDate", "");
                        break;
                    case "invalid_cvc":
                        modelState.AddModelError("CVC", "");
                        break;
                    case "invalid_swipe_data":
                        goto default;
                    case "incorrect_number":
                        modelState.AddModelError("CardNumber", "");
                        break;
                    case "expired_card":
                        modelState.AddModelError("CardNumber", "");
                        break;
                    case "incorrect_cvc":
                        modelState.AddModelError("CVC", "");
                        break;
                    case "incorrect_zip":
                        goto default;
                    case "card_declined":
                        modelState.AddModelError("CardNumber", "");
                        break;
                    case "missing":
                        modelState.AddModelError("CardNumber", "");
                        break;
                    case "processing_error":
                        goto default;
                    default:
                        break;
                }

                modelState.AddModelError("", @"Payment Error: " + exception.StripeError.Message);
                throw new RegistrationException(@"Payment Error: " + exception.StripeError.Message);
            }
            catch (Exception exception)
            {
                modelState.AddModelError("", @"Error of implementation Payment: " + exception.Message);
                throw new RegistrationException(@"Error of implementation Payment: " + exception.Message);
            }
        }


        private string CreateStripePlan(RegistrationViewModel registrationData)
        {
            var planOptions = new StripePlanCreateOptions
            {
                Id = Guid.NewGuid().ToString(),
                Amount = _mediator.Amount,
                Currency = "usd",
                Interval = registrationData.BillingPeriod == BillingPeriod.Month ? "month" : "year",
                Nickname = "Plan for RelayWorks user: " + registrationData.Email,
                Product = new StripePlanProductCreateOptions
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Plan for RelayWorks user: " + registrationData.Email
                }
            };

            StripePlan plan = _stripePlanService.Create(planOptions);

            return plan.Id;
        }


        private string CreateStripeUser(RegistrationViewModel registrationData, string planId)
        {
            StripeConfiguration.SetApiKey(StripeConstants.StripeApiKey);

            DateTime dateTime = DateTime.ParseExact(registrationData.ExpDate, "MM/yy",
                   System.Globalization.CultureInfo.InvariantCulture);

            var customerOptions = new StripeCustomerCreateOptions
            {
                Description = registrationData.Name + " " + registrationData.Surname,
                Email = registrationData.Email,
                PlanId = planId,
                BusinessVatId = registrationData.VAT,
                TaxPercent = registrationData.UserCountry != null && registrationData.UserCountry.ToLower() == "united kingdom" ? 20 : 0,
                SourceCard = new SourceCard
                {
                    Cvc = registrationData.CVC,
                    Number = registrationData.CardNumber,
                    ExpirationMonth = dateTime.Month,
                    ExpirationYear = dateTime.Year,
                    Name = registrationData.NameOnCard,
                    Metadata = new Dictionary<string, string>
                    {
                        { "StartDate", registrationData.StartDate },
                        { "IssueNumber", registrationData.IssueNumber },
                        { "Country", registrationData.UserCountry }
                    }
                }
            };

            StripeCustomer customer = _stripeCustomerService.Create(customerOptions);

            return customer.Id;
        }


        private int CalculatePrice(RegistrationViewModel model)
        {
            var pricesConstants = _unitOfWork.SubscriptionPricesConstants;

            int amount = 0;

            switch (model.UserSubscriptionType)
            {
                case SubscriptionType.Single:
                    switch (model.SubscriptionLevelType)
                    {
                        case RestrictionType.Basic:
                            amount = (int)(pricesConstants.UserPriceForBasicType + pricesConstants.FirstManagerPriceForBasicType);
                            break;
                        case RestrictionType.Premium:
                            amount = (int)(pricesConstants.UserPriceForPremiumType + pricesConstants.FirstManagerPriceForPremiumType);
                            break;
                        case RestrictionType.Ultimate:
                            amount = (int)(pricesConstants.UserPriceForUltimateType + pricesConstants.FirstManagerPriceForUltimateType);
                            break;
                    }
                    break;
                case SubscriptionType.BackToBack:
                    switch (model.SubscriptionLevelType)
                    {
                        case RestrictionType.Basic:
                            amount = (int)(pricesConstants.UserPriceForBasicType * 2 + pricesConstants.FirstManagerPriceForBasicType);
                            break;
                        case RestrictionType.Premium:
                            amount = (int)(pricesConstants.UserPriceForPremiumType * 2 + pricesConstants.FirstManagerPriceForPremiumType);
                            break;
                        case RestrictionType.Ultimate:
                            amount = (int)(pricesConstants.UserPriceForUltimateType * 2 + pricesConstants.FirstManagerPriceForUltimateType);
                            break;
                    }
                    break;
                case SubscriptionType.Team:
                    switch (model.SubscriptionLevelType)
                    {
                        case RestrictionType.Basic:
                            amount = (int)(pricesConstants.UserPriceForBasicType * model.SimpleUsersCount + pricesConstants.FirstManagerPriceForBasicType);

                            if (model.ManagersUsersCount >= 2)
                            {
                                amount = (int)(amount + (model.ManagersUsersCount - 1) * pricesConstants.AdditionalManagerPrice);
                            }
                            break;
                        case RestrictionType.Premium:
                            amount = (int)(pricesConstants.UserPriceForPremiumType * model.SimpleUsersCount + pricesConstants.FirstManagerPriceForPremiumType);

                            if (model.ManagersUsersCount >= 2)
                            {
                                amount = (int)(amount + (model.ManagersUsersCount - 1) * pricesConstants.AdditionalManagerPrice);
                            }
                            break;
                        case RestrictionType.Ultimate:
                            amount = (int)(pricesConstants.UserPriceForUltimateType * model.SimpleUsersCount + pricesConstants.FirstManagerPriceForUltimateType);

                            if (model.ManagersUsersCount >= 2)
                            {
                                amount = (int)(amount + (model.ManagersUsersCount - 1) * pricesConstants.AdditionalManagerPrice);
                            }
                            break;
                    }
                    break;
            }

            var billingСoefficient = model.BillingPeriod == BillingPeriod.Month ? 1 : 12;

            return amount * 100 * billingСoefficient;
        }


        private string CreateStripePlan(UpgradeSubscriptionViewModel upgradeData)
        {
            var planOptions = new StripePlanCreateOptions
            {
                Id = Guid.NewGuid().ToString(),
                Amount = _mediator.Amount,
                Currency = "gbp",
                Interval = "month",
                Nickname = "Plan for RelayWorks user: " + upgradeData.Email,
                Product = new StripePlanProductCreateOptions
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Plan for RelayWorks user: " + upgradeData.Email
                }
            };

            StripePlan plan = _stripePlanService.Create(planOptions);

            return plan.Id;
        }


        private string CreateStripeUser(UpgradeSubscriptionViewModel upgradeData, string planId)
        {
            StripeConfiguration.SetApiKey(StripeConstants.StripeApiKey);

            DateTime dateTime = DateTime.ParseExact(upgradeData.ExpDate, "MM/yy",
                   System.Globalization.CultureInfo.InvariantCulture);

            var customerOptions = new StripeCustomerCreateOptions
            {
                Description = "RelayWorks user",
                Email = upgradeData.Email,
                PlanId = planId,
                BusinessVatId = upgradeData.VAT,
                TaxPercent = upgradeData.UserCountry != null && upgradeData.UserCountry.ToLower() == "united kingdom" ? 20 : 0,
                SourceCard = new SourceCard
                {
                    Cvc = upgradeData.CVC,
                    Number = upgradeData.CardNumber,
                    ExpirationMonth = dateTime.Month,
                    ExpirationYear = dateTime.Year
                },
                Metadata = new Dictionary<string, string>
                {

                    { "Country", upgradeData.UserCountry }
                }
            };

            StripeCustomer customer = _stripeCustomerService.Create(customerOptions);

            return customer.Id;
        }


        private int CalculatePrice(UpgradeSubscriptionViewModel model)
        {
            var pricesConstants = _unitOfWork.SubscriptionPricesConstants;

            int amount = 0;

            switch (model.SubscriptionLevelType)
            {
                case SubscriptionRestrictionType.Basic:
                    amount = (int)(pricesConstants.UserPriceForBasicType * model.NumberOfusers + pricesConstants.FirstManagerPriceForBasicType);
                    break;
                case SubscriptionRestrictionType.Premium:
                    amount = (int)(pricesConstants.UserPriceForPremiumType * model.NumberOfusers + pricesConstants.FirstManagerPriceForPremiumType);
                    break;
                case SubscriptionRestrictionType.Ultimate:
                    amount = (int)(pricesConstants.UserPriceForUltimateType * model.NumberOfusers + pricesConstants.FirstManagerPriceForUltimateType);
                    break;
            }

            return amount * 100;
        }


        #endregion



        #region Trial

        public override void Run(RegistrationTrialViewModel registrationData, ModelStateDictionary modelState)
        {
            try
            {
                _mediator.Amount = CalculatePrice(registrationData);
                _mediator.CreatedStripePlanId = CreateStripePlan(registrationData);
                _mediator.CreatedStripeUserId = CreateStripeUser(registrationData, _mediator.CreatedStripePlanId);
            }
            catch (Exception exception)
            {
                modelState.AddModelError("", @"Error of implementation Payment: " + exception.Message);
                throw new RegistrationException(@"Error of implementation Payment: " + exception.Message);
            }
        }


        private string CreateStripePlan(RegistrationTrialViewModel registrationData)
        {
            var planOptions = new StripePlanCreateOptions
            {
                Id = Guid.NewGuid().ToString(),
                Amount = _mediator.Amount,
                Currency = "gbp",
                Interval = "month",
                //  TrialPeriodDays = 14,
                Nickname = "Plan for RelayWorks user: " + registrationData.Email,
                Product = new StripePlanProductCreateOptions
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Plan for RelayWorks user: " + registrationData.Email,
                }
            };

            StripePlan plan = _stripePlanService.Create(planOptions);

            return plan.Id;
        }


        private string CreateStripeUser(RegistrationTrialViewModel registrationData, string planId)
        {
            StripeConfiguration.SetApiKey(StripeConstants.StripeApiKey);


            var customerOptions = new StripeCustomerCreateOptions
            {
                Description = "RelayWorks user",
                Email = registrationData.Email,
                PlanId = planId,
                TrialEnd = DateTime.UtcNow.AddDays(14)
            };

            StripeCustomer customer = _stripeCustomerService.Create(customerOptions);

            return customer.Id;
        }


        private int CalculatePrice(RegistrationTrialViewModel model)
        {
            var pricesConstants = _unitOfWork.SubscriptionPricesConstants;

            int amount = 0;

            switch (model.UserSubscriptionType)
            {
                case SubscriptionType.Single:
                    switch (model.SubscriptionLevelType)
                    {
                        case RestrictionType.Basic:
                            amount = (int)(pricesConstants.UserPriceForBasicType + pricesConstants.FirstManagerPriceForBasicType);
                            break;
                        case RestrictionType.Premium:
                            amount = (int)(pricesConstants.UserPriceForPremiumType + pricesConstants.FirstManagerPriceForPremiumType);
                            break;
                        case RestrictionType.Ultimate:
                            amount = (int)(pricesConstants.UserPriceForUltimateType + pricesConstants.FirstManagerPriceForUltimateType);
                            break;
                    }
                    break;
                case SubscriptionType.BackToBack:
                    switch (model.SubscriptionLevelType)
                    {
                        case RestrictionType.Basic:
                            amount = (int)(pricesConstants.UserPriceForBasicType * 2 + pricesConstants.FirstManagerPriceForBasicType);
                            break;
                        case RestrictionType.Premium:
                            amount = (int)(pricesConstants.UserPriceForPremiumType * 2 + pricesConstants.FirstManagerPriceForPremiumType);
                            break;
                        case RestrictionType.Ultimate:
                            amount = (int)(pricesConstants.UserPriceForUltimateType * 2 + pricesConstants.FirstManagerPriceForUltimateType);
                            break;
                    }
                    break;
                case SubscriptionType.Team:
                    switch (model.SubscriptionLevelType)
                    {
                        case RestrictionType.Basic:
                            amount = (int)(pricesConstants.UserPriceForBasicType * model.SimpleUsersCount + pricesConstants.FirstManagerPriceForBasicType);

                            if (model.ManagersUsersCount >= 2)
                            {
                                amount = (int)(amount + (model.ManagersUsersCount - 1) * pricesConstants.AdditionalManagerPrice);
                            }
                            break;
                        case RestrictionType.Premium:
                            amount = (int)(pricesConstants.UserPriceForPremiumType * model.SimpleUsersCount + pricesConstants.FirstManagerPriceForPremiumType);

                            if (model.ManagersUsersCount >= 2)
                            {
                                amount = (int)(amount + (model.ManagersUsersCount - 1) * pricesConstants.AdditionalManagerPrice);
                            }
                            break;
                        case RestrictionType.Ultimate:
                            amount = (int)(pricesConstants.UserPriceForUltimateType * model.SimpleUsersCount + pricesConstants.FirstManagerPriceForUltimateType);

                            if (model.ManagersUsersCount >= 2)
                            {
                                amount = (int)(amount + (model.ManagersUsersCount - 1) * pricesConstants.AdditionalManagerPrice);
                            }
                            break;
                    }
                    break;
            }

            var billingСoefficient = model.BillingPeriod == BillingPeriod.Month ? 1 : 12;

            return amount * 100 * billingСoefficient;
        }

        #endregion

    }
}