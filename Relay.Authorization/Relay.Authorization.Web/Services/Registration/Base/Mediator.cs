﻿using System;
using System.Web.Mvc;
using MomentumPlus.Relay.Authorization.Domain.Entities;
using MomentumPlus.Relay.Authorization.Models;
using MomentumPlus.Relay.Models;
using RegistrationViewModel = MomentumPlus.Relay.Authorization.Models.RegistrationViewModel;

namespace MomentumPlus.Relay.Authorization.Services
{
    abstract class Mediator
    {
        public int Amount { get; set; }

        public string CreatedStripePlanId { get; set; }

        public string CreatedStripeUserId { get; set; }

        public RelayUser CreatedUser { get; set; }

        public Guid? CreatedSubscriptionId { get; set; }

        public abstract void Execute(RegistrationViewModel registrationData, ModelStateDictionary modelState, RegistrationStep registrationStep);
        public abstract void Execute(RegistrationTrialViewModel registrationData, ModelStateDictionary modelState, RegistrationStep registrationStep);

        public abstract void Execute(UpgradeSubscriptionViewModel upgradeData, ModelStateDictionary modelState, RegistrationStep registrationStep);

    }
}