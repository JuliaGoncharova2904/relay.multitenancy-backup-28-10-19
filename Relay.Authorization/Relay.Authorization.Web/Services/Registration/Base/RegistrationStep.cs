﻿using System.Web.Mvc;
using MomentumPlus.Relay.Authorization.Models;
using MomentumPlus.Relay.Models;
using RegistrationViewModel = MomentumPlus.Relay.Authorization.Models.RegistrationViewModel;

namespace MomentumPlus.Relay.Authorization.Services
{
    abstract class RegistrationStep
    {
        protected Mediator Mediator;

        protected RegistrationStep(Mediator mediator)
        {
            this.Mediator = mediator;
        }

        public virtual void Execute(RegistrationViewModel registrationData, ModelStateDictionary modelState)
        {
            Mediator.Execute(registrationData, modelState, this);
        }
        public abstract void Run(RegistrationViewModel registrationData, ModelStateDictionary modelState);


        public virtual void Execute(UpgradeSubscriptionViewModel upgradeData, ModelStateDictionary modelState)
        {
            Mediator.Execute(upgradeData, modelState, this);
        }

        public abstract void Run(UpgradeSubscriptionViewModel upgradeData, ModelStateDictionary modelState);


        public virtual void Execute(RegistrationTrialViewModel registrationData, ModelStateDictionary modelState)
        {
            Mediator.Execute(registrationData, modelState, this);
        }
        public abstract void Run(RegistrationTrialViewModel registrationData, ModelStateDictionary modelState);

        public abstract void Rollback();


        public abstract void Update();
    }
}