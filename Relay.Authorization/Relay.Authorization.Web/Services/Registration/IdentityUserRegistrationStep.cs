﻿using System;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MomentumPlus.Relay.Authorization.Auth.Managers;
using MomentumPlus.Relay.Authorization.Domain.Entities;
using MomentumPlus.Relay.Authorization.Models;
using MomentumPlus.Relay.Models;
using RegistrationViewModel = MomentumPlus.Relay.Authorization.Models.RegistrationViewModel;

namespace MomentumPlus.Relay.Authorization.Services.Registration
{
    class IdentityUserRegistrationStep : RegistrationStep
    {
        private RelayUserManager UserManager => HttpContext.Current.GetOwinContext().Get<RelayUserManager>();

        private readonly Mediator _mediator;

        public IdentityUserRegistrationStep(Mediator mediator) : base(mediator)
        {
            this._mediator = mediator;
        }


        public override void Run(RegistrationViewModel registrationData, ModelStateDictionary modelState)
        {
            CheckIfUserExist(registrationData.Email, modelState);

            CreateIdentityUser(registrationData, modelState);
        }

        public override void Run(UpgradeSubscriptionViewModel upgradeData, ModelStateDictionary modelState)
        {
            throw new NotImplementedException();
        }

        public override void Run(RegistrationTrialViewModel registrationData, ModelStateDictionary modelState)
        {
            CheckIfUserExist(registrationData.Email, modelState);

            CreateIdentityUser(registrationData, modelState);
        }

        public override void Rollback()
        {
            var createdUser = _mediator.CreatedUser;

            if (createdUser != null)
            {
                createdUser.SubscriptionId = null;
                createdUser.Subscription = null;

                UserManager.Update(createdUser);
                UserManager.Delete(createdUser);
            }
        }

        public override void Update()
        {
            _mediator.CreatedUser.SubscriptionId = _mediator.CreatedSubscriptionId;

            var updateResult = UserManager.Update(_mediator.CreatedUser);

            if (!updateResult.Succeeded)
            {
                throw new RegistrationException("Error when update Identity User");
            }
        }

        private void CheckIfUserExist(string email, ModelStateDictionary modelState)
        {
            var existUser = UserManager.FindByEmail(email);

            if (existUser != null)
            {
                modelState.AddModelError("Email", @"The user with this email already exist. You can log in by clicking on this <a href=\" + "Account/Login?userId=" + existUser.Id + " target=\"_blank\">link.</a>");
               // modelState.AddModelError("Email", @"The user with this email already exist.");
                throw new RegistrationException(@"The user with this email already exist.You can log in by clicking on this <a href =\" + "Account/Login?userId=" + existUser.Id + " target =\"_blank\">link.</a>");
            }
        }

        private void CreateIdentityUser(RegistrationViewModel registrationData, ModelStateDictionary modelState)
        {
            var user = new RelayUser
            {
                UserName = registrationData.Email,
                Email = registrationData.Email,
                SubscriptionId = null,
                Id = Guid.NewGuid(),
                LockoutEnabled = false,
                EmailConfirmed = false
            };

            try
            {
                UserManager.Create(user, registrationData.Password);

                if (!Boolean.Parse(WebConfigurationManager.AppSettings["isInTestMode"]))
                {
                    this._mediator.CreatedUser = user;
                    user.EmailConfirmed = false;
                }
                else
                {
                    this._mediator.CreatedUser = user;
                    user.EmailConfirmed = true;
                }
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", @"Error when create User.");
                throw new RegistrationException(@"Error when create User. " + ex.Message);
            }
        }

        private void CreateIdentityUser(RegistrationTrialViewModel registrationData, ModelStateDictionary modelState)
        {
            var user = new RelayUser
            {
                UserName = registrationData.Email,
                Email = registrationData.Email,
                SubscriptionId = null,
                Id = Guid.NewGuid(),
                LockoutEnabled = false,
                EmailConfirmed = false
            };

            try
            {
                UserManager.Create(user, registrationData.Password);

                if (!Boolean.Parse(WebConfigurationManager.AppSettings["isInTestMode"]))
                {
                    this._mediator.CreatedUser = user;
                    user.EmailConfirmed = false;
                }
                else
                {
                    this._mediator.CreatedUser = user;
                    user.EmailConfirmed = true;
                }
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", @"Error when create User.");
                throw new RegistrationException(@"Error when create User. " + ex.Message);
            }
        }
    }
}