﻿using System;
using System.Web.Mvc;
using MomentumPlus.Relay.Helpers;
using MomentumPlus.Relay.Models;
using NLog;
using RegistrationViewModel = MomentumPlus.Relay.Authorization.Models.RegistrationViewModel;

namespace MomentumPlus.Relay.Authorization.Services.Registration
{
    class InitializeRelayRegistrationStep : RegistrationStep
    {
        private readonly Mediator _mediator;

        private static readonly Logger Logger = LogManager.GetLogger("MultiTenancy.Registration.Success");

        public InitializeRelayRegistrationStep(Mediator mediator) : base(mediator)
        {
            this._mediator = mediator;
        }

        public override void Run(Models.RegistrationViewModel registrationData, ModelStateDictionary modelState)
        {
            try
            {
                AppSession.Current.Subscription = new SubscriptionDetailsModel
                {
                    ManagerCompany = registrationData.Company,
                    ManagerId = _mediator.CreatedUser.Id,
                    ManagerName = registrationData.Name,
                    ManagerPosition = registrationData.Position,
                    ManagerSurname = registrationData.Surname,
                    ManagerEmail = registrationData.Email,
                    ManagerOfficeLocation = registrationData.OfficeLocation,
                    TimeZone = registrationData.TimeZone
                };

                AppSession.Current.CurrentSubscriptionId = _mediator.CreatedSubscriptionId;

                Logger.Info($"Registration Success. New Subscription Id - {0}. Manager - {1}. Email - {2}.",
                    AppSession.Current.CurrentSubscriptionId,
                    AppSession.Current.Subscription.ManagerName,
                    AppSession.Current.Subscription.ManagerEmail);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", @"Error populate SubscriptionDetailsModel.");
                throw new RegistrationException(@"Error populate SubscriptionDetailsModel. " + ex.Message);
            }
        }

        public override void Run(UpgradeSubscriptionViewModel upgradeData, ModelStateDictionary modelState)
        {
            throw new NotImplementedException();
        }


        public override void Run(Models.RegistrationTrialViewModel registrationData, ModelStateDictionary modelState)
        {
            try
            {
                AppSession.Current.Subscription = new SubscriptionDetailsModel
                {
                    ManagerCompany = registrationData.Company,
                    ManagerId = _mediator.CreatedUser.Id,
                    ManagerName = registrationData.Name,
                    ManagerPosition = registrationData.Position,
                    ManagerSurname = registrationData.Surname,
                    ManagerEmail = registrationData.Email,
                    ManagerOfficeLocation = registrationData.OfficeLocation,
                    TimeZone = registrationData.TimeZone
                };

                AppSession.Current.CurrentSubscriptionId = _mediator.CreatedSubscriptionId;

                Logger.Info($"Registration Success. New Subscription Id - {0}. Manager - {1}. Email - {2}.",
                    AppSession.Current.CurrentSubscriptionId,
                    AppSession.Current.Subscription.ManagerName,
                    AppSession.Current.Subscription.ManagerEmail);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", @"Error populate SubscriptionDetailsModel.");
                throw new RegistrationException(@"Error populate SubscriptionDetailsModel. " + ex.Message);
            }
        }

        public override void Rollback()
        {
        }

        public override void Update()
        {
        }
    }
}