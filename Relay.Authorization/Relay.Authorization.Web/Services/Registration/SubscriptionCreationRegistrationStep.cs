﻿using System;
using System.Web.Mvc;
using MomentumPlus.Relay.Authorization.Models;
using MomentumPlus.Relay.Models;
using RegistrationViewModel = MomentumPlus.Relay.Authorization.Models.RegistrationViewModel;

namespace MomentumPlus.Relay.Authorization.Services.Registration
{
    class SubscriptionCreationRegistrationStep : RegistrationStep
    {
        private readonly SubscriptionService _subscriptionService;

        private readonly Mediator _mediator;


        public SubscriptionCreationRegistrationStep(Mediator mediator) : base(mediator)
        {
            this._subscriptionService = new SubscriptionService();
            this._mediator = mediator;
        }

        public override void Run(RegistrationViewModel registrationData, ModelStateDictionary modelState)
        {
            try
            {
                Mediator.CreatedSubscriptionId = _subscriptionService.CreateSubscription(_mediator.CreatedUser.Id,
                                                                                            registrationData,
                                                                                            _mediator.CreatedStripeUserId,
                                                                                            _mediator.CreatedStripePlanId,
                                                                                            _mediator.Amount,
                                                                                            true);
            }
            catch (Exception ex)
            {
                var error = ex.ToString();
                throw new RegistrationException(@"Error when create Subscription");
            }
        }

        public override void Run(UpgradeSubscriptionViewModel upgradeData, ModelStateDictionary modelState)
        {
            _subscriptionService.UpdateTrialSubscription(upgradeData, _mediator.CreatedStripeUserId,
                _mediator.CreatedStripePlanId,
                _mediator.Amount);
        }


        public override void Run(RegistrationTrialViewModel registrationData, ModelStateDictionary modelState)
        {
            try
            {
                Mediator.CreatedSubscriptionId = _subscriptionService.CreateTrialSubscription(_mediator.CreatedUser.Id,
                                                                                            registrationData,
                                                                                            _mediator.CreatedStripeUserId,
                                                                                            _mediator.CreatedStripePlanId,
                                                                                            _mediator.Amount,
                                                                                            true);
            }
            catch (Exception)
            {

                throw new RegistrationException(@"Error when create Subscription");
            }
        }

        public override void Rollback()
        {
            var createdSubscriptionId = _mediator.CreatedSubscriptionId;

            if (createdSubscriptionId.HasValue)
            {
                _subscriptionService.DeleteSubscription(createdSubscriptionId.Value);
            }
        }

        public override void Update()
        {
        }
    }
}