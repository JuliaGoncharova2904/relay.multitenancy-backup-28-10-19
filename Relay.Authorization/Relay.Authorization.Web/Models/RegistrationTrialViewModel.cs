﻿using System;
using System.ComponentModel.DataAnnotations;
using MomentumPlus.Relay.Authorization.Domain.Entities;

namespace MomentumPlus.Relay.Authorization.Models
{
    public class RegistrationTrialViewModel
    {
        [Required]
        [Display(Name = "E-mail*")]
        [EmailAddress]
        [StringLength(256, MinimumLength = 5, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Surname*")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Surname { get; set; }


        [Required]
        [Display(Name = "Company*")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Company { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string OfficeLocation { get; set; }

        [Required]
        [Display(Name = "Job Position*")]
        [StringLength(50, MinimumLength = 3)]
        public string Position { get; set; }

        [Required]
        [Display(Name = "Password*")]
        [RegularExpression(
            @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$",
            ErrorMessage = @"Your password must be 8 characters and contain a mixture of upper and lower-case letters, numbers and special characters such as !, #, @ or ?"
        )]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "The passwords do not match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password*")]
        public string PasswordConfirm { get; set; }

        [Required]
        public int SimpleUsersCount { get; set; }

        [Required]
        public int ManagersUsersCount { get; set; }


        [Required]
        public SubscriptionType UserSubscriptionType { get; set; }


        [Required]
        public RestrictionType SubscriptionLevelType { get; set; }


        [Required]
        public BillingPeriod BillingPeriod { get; set; }

        [Required]
        public bool IsAgree { get; set; }

        [Required(ErrorMessage = "Please select TimeZone")]
        [Display(Name = "TimeZone*")]
        public string TimeZone { get; set; }
    }
}
