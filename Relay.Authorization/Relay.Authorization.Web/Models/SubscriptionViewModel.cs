﻿using MomentumPlus.Relay.Authorization.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Authorization.Models
{
    public class SubscriptionViewModel
    {
        public Guid SubscriptionId { get; set; }

        public string ManagerEmail { get; set; }

        public string SubscriptionType { get; set; }

        public string RestrictionType { get; set; }

        public string BillingPeriod { get; set; }

        public string CreatedDate { get; set; }

        public string EndDate { get; set; }

        public string Users { get; set; }

        public DateTime? EndDateTime { get; set; }

        public StatusOfSubscription SubscriptionStatus { get; set; }

        public string TimeZone { get; set; }
    }
}
