namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTimeZone : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.Subscriptions", "TimeZone", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.Subscriptions", "TimeZone");
        }
    }
}
