// <auto-generated />
namespace MomentumPlus.Relay.Authorization.Domain
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class EmailOwnerRotation : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(EmailOwnerRotation));
        
        string IMigrationMetadata.Id
        {
            get { return "201909101221481_EmailOwnerRotation"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
