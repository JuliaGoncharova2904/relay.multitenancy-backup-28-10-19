namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.RelayRotations", "SubscriptionId", c => c.Guid());
            AddColumn("multiTenancy.RelayShifts", "SubscriptionId", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.RelayShifts", "SubscriptionId");
            DropColumn("multiTenancy.RelayRotations", "SubscriptionId");
        }
    }
}
