namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewTableIsShift : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.RelayRotationModules", "IsShift", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.RelayRotationModules", "IsShift");
        }
    }
}
