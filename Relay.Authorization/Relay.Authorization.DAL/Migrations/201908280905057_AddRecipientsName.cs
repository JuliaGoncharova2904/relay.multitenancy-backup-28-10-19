namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRecipientsName : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.RelayRotationTopics", "RecipientsName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.RelayRotationTopics", "RecipientsName");
        }
    }
}
