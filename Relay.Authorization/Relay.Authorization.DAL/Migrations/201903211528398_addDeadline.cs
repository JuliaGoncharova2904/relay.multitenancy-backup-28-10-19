namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDeadline : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.RelayRotationTasks", "Deadline", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.RelayRotationTasks", "Deadline");
        }
    }
}
