namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmailOwnerRotation : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.RelayRotations", "RotationOwnerEmail", c => c.String());
            AddColumn("multiTenancy.RelayShifts", "RotationOwnerEmail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.RelayShifts", "RotationOwnerEmail");
            DropColumn("multiTenancy.RelayRotations", "RotationOwnerEmail");
        }
    }
}
