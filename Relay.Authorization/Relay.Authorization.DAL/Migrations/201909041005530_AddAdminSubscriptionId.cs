namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAdminSubscriptionId : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.RelayRotations", "AdminSubscriptionId", c => c.Guid());
            AddColumn("multiTenancy.RelayShifts", "AdminSubscriptionId", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.RelayShifts", "AdminSubscriptionId");
            DropColumn("multiTenancy.RelayRotations", "AdminSubscriptionId");
        }
    }
}
