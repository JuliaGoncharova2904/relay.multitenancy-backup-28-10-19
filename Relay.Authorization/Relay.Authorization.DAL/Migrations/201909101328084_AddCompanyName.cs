namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCompanyName : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.RelayRotations", "CompanyName", c => c.String());
            AddColumn("multiTenancy.RelayShifts", "CompanyName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.RelayShifts", "CompanyName");
            DropColumn("multiTenancy.RelayRotations", "CompanyName");
        }
    }
}
