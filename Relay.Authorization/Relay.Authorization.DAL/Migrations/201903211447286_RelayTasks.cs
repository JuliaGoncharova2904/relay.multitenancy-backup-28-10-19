namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RelayTasks : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "multiTenancy.RelayRotationTasks",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Reference = c.String(),
                        Description = c.String(),
                        Priority = c.String(),
                        IsNullReport = c.Boolean(nullable: false),
                        Date = c.String(),
                        IsPinned = c.Boolean(),
                        RelayRotationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("multiTenancy.RelayRotationTasks");
        }
    }
}
