namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "multiTenancy.RelayUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("multiTenancy.RelayUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "multiTenancy.RelayUsers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SubscriptionId = c.Guid(),
                        LastLoginDate = c.DateTime(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("multiTenancy.Subscriptions", t => t.SubscriptionId)
                .Index(t => t.SubscriptionId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "multiTenancy.RelayUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Guid(nullable: false),
                        Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("multiTenancy.RelayUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "multiTenancy.RelayUserRoles",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                        Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("multiTenancy.RelayRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("multiTenancy.RelayUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "multiTenancy.RelayRoles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Description = c.String(),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "multiTenancy.Subscriptions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CreatedDateUTC = c.DateTime(nullable: false),
                        SubscriptionEndDate = c.DateTime(),
                        OwnerId = c.Guid(nullable: false),
                        RelayDbConnection = c.String(),
                        StripePlanId = c.String(),
                        StripeManagerId = c.String(),
                        SubscriptionType = c.Int(nullable: false),
                        RestrictionType = c.Int(nullable: false),
                        BillingPeriod = c.Int(nullable: false),
                        Restrictions_MaxUsers = c.Int(nullable: false),
                        Restrictions_MaxManagers = c.Int(nullable: false),
                        Restrictions_MaxHandoverItems = c.Int(),
                        Restrictions_MaxHandoverTasks = c.Int(),
                        Restrictions_MaxEmailShares = c.Int(),
                        Restrictions_MaxMonthArchives = c.Int(),
                        Restrictions_MaxCarryForwards = c.Int(),
                        Restrictions_CompanyBranding = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("multiTenancy.RelayUsers", t => t.OwnerId)
                .Index(t => t.OwnerId);
            
            CreateTable(
                "multiTenancy.SubscriptionPayments",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        SubscriptionId = c.Guid(nullable: false),
                        DateTimeUTC = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Currency = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("multiTenancy.Subscriptions", t => t.SubscriptionId)
                .Index(t => t.SubscriptionId);
            
            CreateTable(
                "multiTenancy.SubscriptionPricesConstants",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FirstManagerPriceForBasicType = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FirstManagerPriceForPremiumType = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FirstManagerPriceForUltimateType = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AdditionalManagerPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UserPriceForBasicType = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UserPriceForPremiumType = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UserPriceForUltimateType = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("multiTenancy.RelayUsers", "SubscriptionId", "multiTenancy.Subscriptions");
            DropForeignKey("multiTenancy.SubscriptionPayments", "SubscriptionId", "multiTenancy.Subscriptions");
            DropForeignKey("multiTenancy.Subscriptions", "OwnerId", "multiTenancy.RelayUsers");
            DropForeignKey("multiTenancy.RelayUserRoles", "UserId", "multiTenancy.RelayUsers");
            DropForeignKey("multiTenancy.RelayUserRoles", "RoleId", "multiTenancy.RelayRoles");
            DropForeignKey("multiTenancy.RelayUserLogins", "UserId", "multiTenancy.RelayUsers");
            DropForeignKey("multiTenancy.RelayUserClaims", "UserId", "multiTenancy.RelayUsers");
            DropIndex("multiTenancy.SubscriptionPayments", new[] { "SubscriptionId" });
            DropIndex("multiTenancy.Subscriptions", new[] { "OwnerId" });
            DropIndex("multiTenancy.RelayRoles", "RoleNameIndex");
            DropIndex("multiTenancy.RelayUserRoles", new[] { "RoleId" });
            DropIndex("multiTenancy.RelayUserRoles", new[] { "UserId" });
            DropIndex("multiTenancy.RelayUserLogins", new[] { "UserId" });
            DropIndex("multiTenancy.RelayUsers", "UserNameIndex");
            DropIndex("multiTenancy.RelayUsers", new[] { "SubscriptionId" });
            DropIndex("multiTenancy.RelayUserClaims", new[] { "UserId" });
            DropTable("multiTenancy.SubscriptionPricesConstants");
            DropTable("multiTenancy.SubscriptionPayments");
            DropTable("multiTenancy.Subscriptions");
            DropTable("multiTenancy.RelayRoles");
            DropTable("multiTenancy.RelayUserRoles");
            DropTable("multiTenancy.RelayUserLogins");
            DropTable("multiTenancy.RelayUsers");
            DropTable("multiTenancy.RelayUserClaims");
        }
    }
}
