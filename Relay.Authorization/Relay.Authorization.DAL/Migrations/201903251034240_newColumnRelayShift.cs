namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newColumnRelayShift : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("multiTenancy.RelayRotationModules", "RelayShift_Id", "multiTenancy.RelayShifts");
            DropIndex("multiTenancy.RelayRotationModules", new[] { "RelayShift_Id" });
            AddColumn("multiTenancy.RelayRotationTopicGroups", "ShiftId", c => c.Guid(nullable: false));
            AddColumn("multiTenancy.RelayShifts", "DefaultBackToBackName", c => c.String());
            AddColumn("multiTenancy.RelayShifts", "RotationOwnerName", c => c.String());
            DropColumn("multiTenancy.RelayRotationModules", "RelayShift_Id");
        }
        
        public override void Down()
        {
            AddColumn("multiTenancy.RelayRotationModules", "RelayShift_Id", c => c.Guid());
            DropColumn("multiTenancy.RelayShifts", "RotationOwnerName");
            DropColumn("multiTenancy.RelayShifts", "DefaultBackToBackName");
            DropColumn("multiTenancy.RelayRotationTopicGroups", "ShiftId");
            CreateIndex("multiTenancy.RelayRotationModules", "RelayShift_Id");
            AddForeignKey("multiTenancy.RelayRotationModules", "RelayShift_Id", "multiTenancy.RelayShifts", "Id");
        }
    }
}
