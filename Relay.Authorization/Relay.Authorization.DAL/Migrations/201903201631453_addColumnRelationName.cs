namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addColumnRelationName : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.RelayRotationTopics", "RelationTopicGroupName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.RelayRotationTopics", "RelationTopicGroupName");
        }
    }
}
