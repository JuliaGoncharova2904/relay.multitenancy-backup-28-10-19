namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPriorityTask : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.RelayRotationTasks", "PriorityOfRelayTask", c => c.Int(nullable: false));
            DropColumn("multiTenancy.RelayRotationTasks", "Priority");
            DropColumn("multiTenancy.RelayRotationTasks", "Date");
        }
        
        public override void Down()
        {
            AddColumn("multiTenancy.RelayRotationTasks", "Date", c => c.String());
            AddColumn("multiTenancy.RelayRotationTasks", "Priority", c => c.String());
            DropColumn("multiTenancy.RelayRotationTasks", "PriorityOfRelayTask");
        }
    }
}
