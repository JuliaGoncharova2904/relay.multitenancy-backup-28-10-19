namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnRelayLogoFileNew : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.RelayLogoFiles", "RotationId", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.RelayLogoFiles", "RotationId");
        }
    }
}
