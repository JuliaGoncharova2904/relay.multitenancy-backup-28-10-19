namespace MomentumPlus.Relay.Authorization.Domain
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("multiTenancy.AdminTemplateTopicGroups", "Type", c => c.Int(nullable: false));
            AddColumn("multiTenancy.AdminTemplateTopics", "Type", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("multiTenancy.AdminTemplateTopics", "Type");
            DropColumn("multiTenancy.AdminTemplateTopicGroups", "Type");
        }
    }
}
