﻿using System;

namespace MomentumPlus.Relay.Authorization.Domain
{
    public static class IdentityInitializerConstants
    {
        public static class Users
        {
            public static readonly Guid iHandoverAdminUserId = new Guid("D18E8B72-1756-4CC4-BBED-855153269071");

        }

        public static class Roles
        {
            public static readonly Guid iHandoverAdminRoleId = new Guid("AB66F022-A24B-4780-AE01-284C56C7DC69");

            public static readonly Guid AdminRoleRoleId = new Guid("29D3D119-00B3-4B17-8BDB-C4E64D753465");

            public static readonly Guid HeadLineManagerRoleId = new Guid("4AA14D72-AED5-4A3C-948B-E59964FD858A");

            public static readonly Guid LineManagerRoleId = new Guid("E334244D-F8D9-4A42-86DF-E1D11927182D");

            public static readonly Guid SafetyManagerRoleId = new Guid("AB66F022-A24B-2780-AE01-284C56C7DC19");

            public static readonly Guid ContributorRoleId = new Guid("AB66F022-A24B-4780-AE01-284C56C7DC12");

            public static readonly Guid SimpleUserRoleId = new Guid("AB66F022-A24B-4780-AE01-284C56C7DC19");

            public static readonly Guid ExecutiveUserRoleId = new Guid("1DF12A73-FCF2-435B-A95E-4B5B7D0514D4");
        }


        public static class UserRoles
        {
            public static readonly Guid iHandoverAdminUserRoleId = new Guid("D18E8B72-1756-7CC7-BBED-855153269071");
        }


        public static class SubscriptionPricesConstants
        {
            public static readonly Guid SubscriptionPricesConstantsId = new Guid("E18E8B72-1756-7CC7-BBED-862153267071");
        }
    }
}
