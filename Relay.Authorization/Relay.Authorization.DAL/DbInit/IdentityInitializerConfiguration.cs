﻿using System.Data.Entity.Migrations;

namespace MomentumPlus.Relay.Authorization.Domain
{
    public sealed class IdentityInitializerConfiguration : DbMigrationsConfiguration<MultiTenancyContext>
    {
        public IdentityInitializerConfiguration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(MultiTenancyContext context)
        {
            IdentityInitializer.InitializeRoles(context);

            IdentityInitializer.InitializeUsers(context);

            IdentityInitializer.InitializeUserRoles(context);

            IdentityInitializer.InitializeSubscriptionPricesConstants(context);

            IdentityInitializer.InitializeBaseModules(context);
        }

    }
}
