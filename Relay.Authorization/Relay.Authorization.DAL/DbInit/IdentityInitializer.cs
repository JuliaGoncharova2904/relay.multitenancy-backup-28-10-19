﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using MomentumPlus.Relay.Authorization.Domain.Entities;

namespace MomentumPlus.Relay.Authorization.Domain
{

    public class IdentityInitializer : MigrateDatabaseToLatestVersion<MultiTenancyContext, IdentityInitializerConfiguration>
    {
        public override void InitializeDatabase(MultiTenancyContext context)
        {
        }

        public static void InitializeSubscriptionPricesConstants(MultiTenancyContext context)
        {
            context.SubscriptionPricesConstants.AddOrUpdate(new SubscriptionPricesConstants
            {
                Id = IdentityInitializerConstants.SubscriptionPricesConstants.SubscriptionPricesConstantsId,
                FirstManagerPriceForBasicType = 0,
                FirstManagerPriceForPremiumType = 0,
                FirstManagerPriceForUltimateType = 0,
                AdditionalManagerPrice = 0,
                UserPriceForBasicType = 12,
                UserPriceForPremiumType = 24,
                UserPriceForUltimateType = 30
            });
        }


        public static void InitializeRoles(MultiTenancyContext context)
        {
            RelayRole[] roles = {
                new RelayRole { Id = IdentityInitializerConstants.Roles.AdminRoleRoleId, Name = "Administrator" },
                new RelayRole { Id = IdentityInitializerConstants.Roles.iHandoverAdminRoleId, Name = "iHandover Admin" },
                new RelayRole { Id = IdentityInitializerConstants.Roles.LineManagerRoleId, Name = "Line Manager" },
                new RelayRole { Id = IdentityInitializerConstants.Roles.SimpleUserRoleId, Name = "User" },
                new RelayRole { Id = IdentityInitializerConstants.Roles.ContributorRoleId, Name = "Contributor" },
                new RelayRole { Id = IdentityInitializerConstants.Roles.SafetyManagerRoleId, Name = "Safety Manager"},
                new RelayRole { Id = IdentityInitializerConstants.Roles.HeadLineManagerRoleId, Name = "Head Line Manager"},
                new RelayRole { Id = IdentityInitializerConstants.Roles.ExecutiveUserRoleId, Name = "Executive User"}
            };

            context.Roles.AddOrUpdate(r => r.Id, roles);
        }

        public static void InitializeUsers(MultiTenancyContext context)
        {
            CreateUser(context,
                IdentityInitializerConstants.Users.iHandoverAdminUserId,
                "admin@ihandover.co",
                "admin@ihandover.co");
        }


        public static void InitializeUserRoles(MultiTenancyContext context)
        {

            RelayUserRole[] userRoles = {
                new RelayUserRole
                {
                    Id = IdentityInitializerConstants.UserRoles.iHandoverAdminUserRoleId,
                    RoleId = IdentityInitializerConstants.Roles.iHandoverAdminRoleId,
                    UserId = IdentityInitializerConstants.Users.iHandoverAdminUserId
                }
            };

            context.RelayUserRoles.AddOrUpdate(r => r.Id, userRoles);
        }

        public static void InitializeBaseModules(MultiTenancyContext context)
        {
            AdminTemplateModule[] baseModules = {
                new AdminTemplateModule { Id = new Guid("118E8B72-1756-4CC4-BBED-855153269071"), Type = TypeOfModule.HSE, Enabled = true },
                new AdminTemplateModule { Id = new Guid("218E8B72-1756-4CC4-BBED-855153269072"), Type = TypeOfModule.Core, Enabled = true }
            };

            context.AdminTemplateModule.AddOrUpdate(m => m.Id, baseModules);
        }


        #region Private Methods

        private static void CreateUser(MultiTenancyContext context,
            Guid userId,
            string username,
            string email)
        {

            context.Users.AddOrUpdate(new RelayUser
            {
                Id = userId,
                UserName = username,
                PasswordHash = "ANZtVV4oh6nqD+Zf2p4oJ1NrQXImWXsQ7o7Zt4x6BSAkdEE6IfZghguXzF4m9anuKA==",
                AccessFailedCount = 0,
                Email = email,
                EmailConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString()
            });
        }

        #endregion


    }

}
