﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    [ComplexType]
    public class SubscriptionRestrictions
    {
        public int MaxUsers { get; set; }

        public int MaxManagers { get; set; }

        public int? MaxHandoverItems { get; set; }

        public int? MaxHandoverTasks { get; set; }

        public int? MaxEmailShares { get; set; }

        public int? MaxMonthArchives { get; set; }

        public int? MaxCarryForwards { get; set; }

        public bool CompanyBranding { get; set; }
    }
}
