﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class AdminTemplates : HandoverItem
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? AdminSectorsId { get; set; }

        public virtual ICollection<AdminSectors> AdminSectors { get; set; }
    }
}
