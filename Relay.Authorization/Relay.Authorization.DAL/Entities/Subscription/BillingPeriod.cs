﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public enum BillingPeriod
    {
        [Description("Month")]
        Month = 0,

        [Description("Year")]
        Year = 1,
    }
}
