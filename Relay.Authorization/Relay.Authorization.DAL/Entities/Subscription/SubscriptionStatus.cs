﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public enum SubscriptionStatus
    {
        [Display(Name = "Active")]
        [Description("Active")]
        Active = 0,

        [Display(Name = "Trialing")]
        [Description("Trialing")]
        Trialing = 1,

        [Display(Name = "Trial End")]
        [Description("Trial End")]
        TrialEnd = 2,

        [Display(Name = "Overdue")]
        [Description("Overdue")]
        PastDue = 3,

        [Display(Name = "Unpaid")]
        [Description("Unpaid")]
        Unpaid = 4,

        [Display(Name = "Canceled")]
        [Description("Canceled")]
        Canceled = 5

    }
}
