﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class AdminSectors : HandoverItem
    {
        public string Name { get; set; }

        public Guid? TemplateId { get; set; }

        public virtual AdminTemplates Template { get; set; }
    }
}
