﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public enum SubscriptionType
    {
        [Description("Single")]
        Single = 0,

        [Description("Back to Back")]
        BackToBack = 1,

        [Description("Team")]
        Team = 2
    }
}
