﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class AdminTemplateTopics : HandoverItem
    {
        [Required]
        [StringLength(150, MinimumLength = 3)]
        public string Name { get; set; }

        public string Description { get; set; }

        public Guid TopicGroupId { get; set; }

        public virtual AdminTemplateTopicGroups TopicGroup { get; set; }

        public Guid? ParentTopicId { get; set; }

        public virtual AdminTemplateTopics ParentTopic { get; set; }

        public bool Enabled { get; set; }

        public string UnitsSelectedType { get; set; }

        public bool? IsExpandPlannedActualField { get; set; }

        public TypeOfModule Type { get; set; }

    }
}
