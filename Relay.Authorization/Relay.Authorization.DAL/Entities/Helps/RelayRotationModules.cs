﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class RelayRotationModules
    {
        public Guid Id { get; set; }

        public DateTime? CreatedUtc { get; set; }

        public DateTime? ModifiedUtc { get; set; }

        public DateTime? DeletedUtc { get; set; }

        public TypeOfModule Type { get; set; }

        public bool Enabled { get; set; }

        public TypeOfModuleSource SourceType { get; set; }

        public Guid? TempateModuleId { get; set; }

        public Guid? RotationId { get; set; }

        public virtual RelayRotation Rotation { get; set; }

        public Guid? ShiftId { get; set; }

        public Guid? ParentModuleId { get; set; }

        public bool? IsShift { get; set; }
    }

    public enum TypeOfModuleSource
    {
        Received = 0,
        Draft = 1
    }
}
