﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public abstract class RelayHandoverItem : RelayBaseModel
    {
        protected RelayBaseModel Clone()
        {
            return Clone<RelayBaseModel>();
        }
        protected T Clone<T>() where T : RelayBaseModel
        {
            Type thisType = this.GetType();
            Type cloneType = thisType;

            //Check if object is Proxy
            if (ObjectContext.GetObjectType(thisType) != thisType)
            {
                cloneType = thisType.BaseType;
            }

            var clone = (RelayBaseModel)Activator.CreateInstance(cloneType);
            if (cloneType != null)
            {
                var cloneProperties = cloneType.GetProperties();

                foreach (var prop in cloneProperties.ToList())
                {
                    if (prop.CanWrite)
                        prop.SetValue(clone, thisType.GetProperty(prop.Name).GetValue(this, null), null);
                }
            }

            // Create new Id
            clone.Id = Guid.NewGuid();

            // Set Dates
            clone.CreatedUtc = DateTime.UtcNow;
            clone.ModifiedUtc = null;
            clone.DeletedUtc = null;

            return (T)clone;
        }
    }
}
