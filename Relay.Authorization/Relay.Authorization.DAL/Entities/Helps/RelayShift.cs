﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class RelayShift
    {
        public Guid Id { get; set; }

        public DateTime? CreatedUtc { get; set; }

        public DateTime? ModifiedUtc { get; set; }

        public DateTime? DeletedUtc { get; set; }

        public DateTime? StartDateTime { get; set; }

        public int WorkMinutes { get; set; }

        public int BreakMinutes { get; set; }

        public int RepeatTimes { get; set; }

        public Guid? ShiftRecipientId { get; set; }

        public Guid RotationId { get; set; }

        public ShiftState State { get; set; }

        public string DefaultBackToBackName { get; set; }

        public string RotationOwnerName { get; set; }

        public string RecipientsName { get; set; }

        public Guid? AdminSubscriptionId { get; set; }

        public Guid? SubscriptionId { get; set; }

        public string RotationOwnerEmail { get; set; }

        public string CompanyName { get; set; }
    }

    public enum ShiftState
    {
        Created = 0,
        Confirmed = 1,
        Break = 2,
        Finished = 3,
    }
}
