﻿namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public enum RelayRotationType
    {
        Swing = 0,
        Shift = 1
    }
}
