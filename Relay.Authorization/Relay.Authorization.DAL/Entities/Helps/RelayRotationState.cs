﻿namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public enum RelayRotationState
    {
        Created = 0,
        Confirmed = 1,
        SwingEnded = 2,
        Expired = 3
    }
}
