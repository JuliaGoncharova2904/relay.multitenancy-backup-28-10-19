﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public class RelayRotationTopicGroup
    {
        public Guid Id { get; set; }

        public DateTime? CreatedUtc { get; set; }

        public DateTime? ModifiedUtc { get; set; }

        public DateTime? DeletedUtc { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? RelationId { get; set; }

        public Guid RotationId { get; set; }

        public Guid ShiftId { get; set; }

        public bool Enabled { get; set; }

        public Guid? ParentRotationTopicGroupId { get; set; }

        public RelayTypeOfModule TypeOfModule { get; set; }

        public Guid RotationModuleId { get; set; }

        public virtual ICollection<RelayRotationTopic> RotationTopics { get; set; }
    }
}

