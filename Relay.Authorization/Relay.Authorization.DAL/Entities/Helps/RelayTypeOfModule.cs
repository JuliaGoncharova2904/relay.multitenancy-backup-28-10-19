﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Authorization.Domain.Entities
{
    public enum RelayTypeOfModule
    {
        [Description("Safety")]
        HSE = 0,

        [Description("Core")]
        Core = 1,

        [Description("Project")]
        Project = 2,

        [Description("Team")]
        Team = 3,

        [Description("Daily Note")]
        DailyNote = 4
    }
}
