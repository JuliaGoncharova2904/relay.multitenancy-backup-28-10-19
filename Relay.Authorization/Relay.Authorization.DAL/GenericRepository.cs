﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Data.Entity;
using MomentumPlus.Relay.Authorization.Domain.Interfaces;

namespace MomentumPlus.Relay.Authorization.Domain
{
    public class GenericRepository<TEntity> : IRepository<TEntity>, IDisposable where TEntity : class
    {
        protected MultiTenancyContext _context = null;
        protected DbSet<TEntity> _dbSet;

        public GenericRepository(MultiTenancyContext context)
        {
            this._context = context;
            _dbSet = this._context.Set<TEntity>();
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbSet;
        }

        public TEntity Get(Guid id)
        {
            TEntity entity = _dbSet.Find(id);

            /*if (entity == null)
            {
                throw new Exception($"{typeof (TEntity).Name} with ID:{id} was not found.");
            }*/

            return entity;
        }

        public void Add(TEntity entity)
        {
            _dbSet.Add(entity);
        }

        public void Update(TEntity entity)
        {
            _dbSet.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }


        public void UpdateRotation(TEntity entity)
        {
            var entry = _context.Entry(entity);

            if (entry.State == EntityState.Detached || entry.State == EntityState.Modified)
            {
                _context.Entry(entity).State = EntityState.Detached;
                _dbSet.Attach(entity);
            }
        }

        public void Delete(Guid id)
        {
            TEntity entity = _dbSet.Find(id);
            if (entity != null)
            {
                _dbSet.Remove(entity);
            }
        }

        public void Delete(TEntity entity)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                _dbSet.Attach(entity);
            }
            _dbSet.Remove(entity);
        }

        //IDisposable implementation
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}